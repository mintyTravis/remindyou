package iris.technology.remindingyou

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.Scope
import com.google.android.gms.common.api.internal.ApiExceptionMapper
import com.google.android.gms.common.internal.ApiExceptionUtil
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import timber.log.Timber
import java.lang.Exception

/**
 * TODO: This should be optimized
 *
 */
class GoogleLogin : AppCompatActivity(), View.OnClickListener, UserEngine.VolleyCallback {

    private val TAG = "SignInActivity"
    private val RC_SIGN_IN = 6454

    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var mStatusTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_google_login)

        mStatusTextView = findViewById(R.id.status)

        findViewById<SignInButton>(R.id.sign_in_button).setOnClickListener{
            signIn()
        }
        findViewById<Button>(R.id.sign_out_button).setOnClickListener{
            signOut()
        }
        findViewById<Button>(R.id.disconnect_button).setOnClickListener{
            revokeAccess()
        }
        findViewById<Button>(R.id.back_to_main).setOnClickListener {
            //if(!this.findNavController(R.id.googleLogin).popBackStack())
                finish()
        }


        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken("173150357767-ie9lque65p9p5jbqhv3ijvo4kfqj2q4f.apps.googleusercontent.com")
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this,gso)

        val signInButton = findViewById<SignInButton>(R.id.sign_in_button)
        signInButton.setSize(SignInButton.SIZE_STANDARD)
        signInButton.setColorScheme(SignInButton.COLOR_LIGHT)
    }

    override fun onStart() {
        super.onStart()
        Timber.i("onStart method called")

        val account = GoogleSignIn.getLastSignedInAccount(this)
        updateUI(account)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN){
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {

        try {
            val account = completedTask.getResult(ApiException::class.java)
            updateUI(account)
        }
        catch (e: ApiException) {
            Timber.w("signInResult:failed code = ${e.statusCode}")
            Toast.makeText(this, "Something went wrong handling sign in result",Toast.LENGTH_SHORT).show()
            updateUI(null)
        }
    }

    private fun signIn(){
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent,RC_SIGN_IN)
    }

    private fun signOut(){
        mGoogleSignInClient.signOut()
            .addOnCanceledListener {
                    updateUI(null)
            }
            .addOnCompleteListener {
                updateUI(null)
            }
        UserEngine.userSignOut()
    }

    private fun revokeAccess() {
        mGoogleSignInClient.revokeAccess()
            .addOnCanceledListener {
                updateUI(null)
            }
            .addOnCompleteListener {
                updateUI(null)
            }
        UserEngine.revokeAccess()
    }

    private fun updateUI(account:GoogleSignInAccount?) {
        Timber.i("updateUI method called")
        if (account != null) {
            mStatusTextView.setText(getString(R.string.signed_in_fmt, account.displayName))
            findViewById<SignInButton>(R.id.sign_in_button).visibility = View.GONE
            findViewById<LinearLayout>(R.id.sign_out_and_disconnect).visibility = View.VISIBLE
            findViewById<Button>(R.id.back_to_main).visibility = View.VISIBLE
            if(UserEngine.returnUserID() == 0) {
                UserEngine.onVolleyReturnListener(this)
                UserEngine.initiateVolley(this)
                UserEngine.initiateUser(account)
            }
            //UserEngine.initiateUser(account)
        }
        else {
            mStatusTextView.setText(R.string.sign_out)
            findViewById<SignInButton>(R.id.sign_in_button).visibility = View.VISIBLE
            findViewById<LinearLayout>(R.id.sign_out_and_disconnect).visibility = View.GONE
            //UserEngine.removeUser(account)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.sign_in_button -> signIn()
            R.id.sign_out_button -> signOut()
            R.id.disconnect_button -> revokeAccess()
        }
    }

    override fun onSuccessResponseUser(result: Boolean) {
        Timber.i("remind_you - onSuccessResponseUser")
        if (result) Toast.makeText(this,"Logged into Remind You!",Toast.LENGTH_SHORT).show()
        else Toast.makeText(this,"Something went Wrong!",Toast.LENGTH_SHORT).show()
    }
}
