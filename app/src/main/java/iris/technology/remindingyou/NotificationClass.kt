package iris.technology.remindingyou

import android.annotation.SuppressLint
import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.JobIntentService
import org.json.JSONArray
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalTime
import org.threeten.bp.ZoneId
import timber.log.Timber
import java.io.File
import java.lang.Exception
import java.nio.charset.Charset
import java.util.*


class NotificationReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {


        if (Build.VERSION.SDK_INT < 26){
            val service = Intent(context, NotificationService::class.java)

            service.putExtra("reason", intent.getStringExtra("reason"))
            service.putExtra("timestamp", intent.getLongExtra("timestamp", 0))
            service.putExtra("notificationID", intent.getIntExtra("notificationID", 0))
            service.putExtra("reminderTitle", intent.getStringExtra("reminderTitle"))
            service.putExtra("reminderDescription", intent.getStringExtra("reminderDescription"))

            context.startService(service)
        }
        else {

            val jobIntentService = Intent(context,NotificationJobService::class.java)
            val notificationJService = NotificationJobService()

            jobIntentService.putExtra("reason", intent.getStringExtra("reason"))
            jobIntentService.putExtra("timestamp", intent.getLongExtra("timestamp", 0))
            jobIntentService.putExtra("notificationID", intent.getIntExtra("notificationID", 0))
            jobIntentService.putExtra("reminderTitle", intent.getStringExtra("reminderTitle"))
            jobIntentService.putExtra("reminderDescription", intent.getStringExtra("reminderDescription"))

            notificationJService.enqueueJob(context,jobIntentService,intent.getIntExtra("notificationID",0))
        }

    }

}

class NotificationReceiverDaily :BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        var timeInMilli: Long
        var r: Reminder
        Timber.i("remind_you - NotificationReceiverDaily run")

        val rFile = File (context!!.filesDir,"reminderFile")

        if (rFile.exists()) {
            if (rFile.length() > 0){
                try {
                    val fileJSONArray = JSONArray(rFile.readText(Charset.defaultCharset()))
                    Timber.i("remind_you - $fileJSONArray")
                    for (i in 0.until(fileJSONArray.length())) {
                        if (fileJSONArray.getJSONObject(i).getInt("ReminderRecurType") > 0){
                            if (LocalDate.parse(fileJSONArray.getJSONObject(i).getString("RecurNextReminder")) == LocalDate.now()) {
                                timeInMilli = LocalTime.parse(fileJSONArray.getJSONObject(i).getString("RecurNoticeTime")).atDate(
                                    LocalDate.now()).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()
                                r = ReminderEngine.createSingleReminder(fileJSONArray.getJSONObject(i))
                                NotificationUtils().setNotification(timeInMilli,context,r)
                            }
                        }
                        else{
                            if (LocalDate.parse(fileJSONArray.getJSONObject(i).getString("CompletionDate")) == LocalDate.now()) {
                                timeInMilli = LocalTime.parse(fileJSONArray.getJSONObject(i).getString("CompletionTime")).atDate(
                                    LocalDate.now()).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()
                                r = ReminderEngine.createSingleReminder(fileJSONArray.getJSONObject(i))
                                Timber.i("remind_you - $timeInMilli")
                                NotificationUtils().setNotification(timeInMilli,context,r)
                            }
                        }
                    }
                }
                catch (er: Exception) {
                    Timber.e("remind_you - $er")
                }
            }
        }
    }
}

class BootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == Intent.ACTION_BOOT_COMPLETED) {

            var timeInMilli: Long
            var r: Reminder
            Timber.i("remind_you - BootReceiver run")
            val rFile = File(context!!.filesDir, "reminderFile")

            if (rFile.exists()) {
                if (rFile.length() > 0) {
                    try {
                        val fileJSONArray = JSONArray(rFile.readText(Charset.defaultCharset()))
                        Timber.i("remind_you - $fileJSONArray")
                        for (i in 0.until(fileJSONArray.length())) {
                            if (fileJSONArray.getJSONObject(i).getInt("ReminderRecurType") > 0) {
                                if (LocalDate.parse(fileJSONArray.getJSONObject(i).getString("RecurNextReminder")) == LocalDate.now()) {
                                    timeInMilli =
                                        LocalTime.parse(fileJSONArray.getJSONObject(i).getString("RecurNoticeTime"))
                                            .atDate(
                                                LocalDate.now()
                                            ).atZone(ZoneId.systemDefault()).toInstant()
                                            .toEpochMilli()
                                    r = ReminderEngine.createSingleReminder(
                                        fileJSONArray.getJSONObject(i)
                                    )
                                    NotificationUtils().setNotification(timeInMilli, context, r)
                                }
                            } else {
                                if (LocalDate.parse(fileJSONArray.getJSONObject(i).getString("CompletionDate")) == LocalDate.now()) {
                                    timeInMilli =
                                        LocalTime.parse(fileJSONArray.getJSONObject(i).getString("CompletionTime"))
                                            .atDate(
                                                LocalDate.now()
                                            ).atZone(ZoneId.systemDefault()).toInstant()
                                            .toEpochMilli()
                                    r = ReminderEngine.createSingleReminder(
                                        fileJSONArray.getJSONObject(i)
                                    )
                                    Timber.i("remind_you - $timeInMilli")
                                    NotificationUtils().setNotification(timeInMilli, context, r)
                                }
                            }
                        }
                    } catch (er: Exception) {
                        Timber.e("remind_you - $er")
                    }
                }
                NotificationUtils().setDailyNotificationCheck(context)
            }
        }
    }
}

class NotificationJobService : JobIntentService() {
    private lateinit var mNotification: Notification

    fun enqueueJob(context: Context,intent: Intent,job_id: Int){
        enqueueWork(context,NotificationJobService::class.java, JOB_ID,intent)
    }

    @SuppressLint("NewApi")
    private fun createChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library

            val context = this.applicationContext
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance)
            notificationChannel.enableVibration(true)
            notificationChannel.setShowBadge(true)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.parseColor("#e8334a")
            notificationChannel.description = "RemindYou"
            notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    companion object {
        const val CHANNEL_ID = "iris.technology.remindingyou.notification_channel"
        const val CHANNEL_NAME = "RemindYou Notifications"
        const val JOB_ID = 34
    }

    override fun onHandleWork(intent: Intent) {
        createChannel()
        Timber.i("onHandleWork - jobIntentService")
        var timestamp: Long = 0
        var mNotificationId = 0
        var title = "No title"
        var message = "No message"

        if (intent.extras != null) {
            timestamp = intent.extras!!.getLong("timestamp")
            mNotificationId = intent.extras!!.getInt("notificationID")
            title = intent.extras!!.getString("reminderTitle", "No Title")
            message = intent.extras!!.getString("reminderDescription","No Description")
        }
        else
        {
            title = "No Title"
            message = "No Description"
        }

        if (timestamp > 0) {


            val context = this.applicationContext
            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            val notifyIntent = Intent(this, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
                .putExtra("ReminderID",mNotificationId)

            val c = Calendar.getInstance()
            c.timeInMillis = timestamp


            val notiPendingIntent = PendingIntent.getActivity(
                this,
                0,
                notifyIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            val res = this.resources
            val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                mNotification = Notification.Builder(this, CHANNEL_ID)
                    // Set the intent that will fire when the user taps the notification
                    .setSmallIcon(R.drawable.remind_you_small_logo)
                    .setContentTitle(title)
                    .setShowWhen(true)
                    .setStyle(
                        Notification.BigTextStyle().bigText(message)
                    )
                    .setContentText(message)
                    .setContentIntent(notiPendingIntent)
                    .setAutoCancel(false).build()

            } else {

                mNotification = Notification.Builder(this)
                    // Set the intent that will fire when the user taps the notification
                    .setContentIntent(notiPendingIntent)
                    .setSmallIcon(R.drawable.notification_icon)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher))
                    .setAutoCancel(true)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentTitle(title)
                    .setStyle(
                        Notification.BigTextStyle()
                            .bigText(message)
                    )
                    .setSound(uri)
                    .setContentText(message).build()

            }

            notificationManager.cancel(mNotificationId)

            //notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            // mNotificationId is a unique int for each notification that you must define
            notificationManager.notify(mNotificationId, mNotification)
        }
    }
}

class NotificationService : IntentService("NotificationService") {
    private lateinit var mNotification: Notification

    @SuppressLint("NewApi")
    private fun createChannel() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library

            val context = this.applicationContext
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance)
            notificationChannel.enableVibration(true)
            notificationChannel.setShowBadge(true)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.parseColor("#e8334a")
            notificationChannel.description = "RemindYou"
            notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            notificationManager.createNotificationChannel(notificationChannel)
        }

    }

    companion object {

        const val CHANNEL_ID = "iris.technology.remindingyou.notification_channel"
        const val CHANNEL_NAME = "RemindYou Notifications"
    }


    override fun onHandleIntent(intent: Intent?) {

        //Create Channel
        createChannel()

        var timestamp: Long = 0
        var mNotificationId = 0
        var title = "No Title"
        var message = "No Description"

        if (intent != null && intent.extras != null) {
            timestamp = intent.extras!!.getLong("timestamp")
            mNotificationId = intent.extras!!.getInt("notificationID")
            title = intent.extras!!.getString("reminderTitle", "No Title")
            message = intent.extras!!.getString("reminderDescription","No Description")
        }
        else
        {
            title = "No Title"
            message = "No Description"
        }

        if (timestamp > 0) {


            val context = this.applicationContext
            val notificationManager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val notifyIntent = Intent(this, NotificationActivity::class.java)

            notifyIntent.putExtra("title", title)
            notifyIntent.putExtra("message", message)
            notifyIntent.putExtra("notification", true)

            notifyIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

            val calendar = Calendar.getInstance()
            calendar.timeInMillis = timestamp


            val pendingIntent = PendingIntent.getActivity(context, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            val res = this.resources
            val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {


                mNotification = Notification.Builder(this, CHANNEL_ID)
                    // Set the intent that will fire when the user taps the notification
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.notification_icon)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher))
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setStyle(Notification.BigTextStyle()
                        .bigText(message))
                    .setContentText(message).build()
            } else {

                mNotification = Notification.Builder(this)
                    // Set the intent that will fire when the user taps the notification
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.notification_icon)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher))
                    .setAutoCancel(true)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentTitle(title)
                    .setStyle(Notification.BigTextStyle()
                        .bigText(message))
                    .setSound(uri)
                    .setContentText(message).build()

            }

            notificationManager.cancel(mNotificationId)

            //notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            // mNotificationId is a unique int for each notification that you must define
            notificationManager.notify(mNotificationId, mNotification)
        }


    }
}

/**
 * NotificationUtils
 * This class handles setting the AlarmManager that will trigger the broadcast receiver in order
 * to set up Notifications for reminders at a certain time.
 *
 */
class NotificationUtils {
    /***
     * @param timeInMilliSeconds - this is the time in miliseconds since epoc day for the notification
     * to go off.
     * @param context - context needed to set the alarm manager.
     * @param reminder - reminder for the notification - this will display in the notification.
     *
     */
    fun setNotification(timeInMilliSeconds: Long, context: Context, reminder: Reminder) {

        //------------  alarm settings start  -----------------//
        if (timeInMilliSeconds > 0) {
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val alarmIntent = Intent(context.applicationContext, NotificationReceiver::class.java) // AlarmReceiver1 = broadcast receiver

            alarmIntent.putExtra("reason", "notification")
            alarmIntent.putExtra("timestamp", timeInMilliSeconds)
            alarmIntent.putExtra("notificationID",reminder.ReminderID)
            alarmIntent.putExtra("reminderTitle",reminder.ReminderTitle)
            alarmIntent.putExtra("reminderDescription",reminder.ReminderDescription)

            val pendingIntent = PendingIntent.getBroadcast(context, reminder.ReminderID, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT)
            alarmManager.set(AlarmManager.RTC_WAKEUP, timeInMilliSeconds, pendingIntent)
            Timber.i("remind_you - created alarmManager")
            Timber.i("remind_you - ${alarmManager}")

        }

        //------------ end of alarm settings  -----------------//

    }

    fun setDailyNotificationCheck(context: Context) {

        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val alarmIntent = Intent(context.applicationContext, NotificationReceiverDaily::class.java)

        val c = Calendar.getInstance()
        c.set(Calendar.DAY_OF_YEAR, c.get(Calendar.DAY_OF_YEAR)+1)
        c.set(Calendar.HOUR_OF_DAY, 5)
        c.set(Calendar.MINUTE, 0)
        c.set(Calendar.SECOND, 0)
        c.set(Calendar.MILLISECOND, 0)

        Timber.i("remind_you - $c")
        val pendingIntent = PendingIntent.getBroadcast(context,0, alarmIntent,PendingIntent.FLAG_CANCEL_CURRENT)
        alarmManager.setRepeating(AlarmManager.RTC, c.timeInMillis,(1000*60*60*24),pendingIntent)
        Timber.i("remind_you - repeating alarm set")
    }
}