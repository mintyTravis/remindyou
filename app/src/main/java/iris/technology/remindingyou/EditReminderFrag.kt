package iris.technology.remindingyou

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import iris.technology.remindingyou.databinding.FragmentEditReminderBinding
import kotlinx.android.synthetic.main.fragment_edit_reminder.*
import org.threeten.bp.*
import timber.log.Timber
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

/**
 * Edit Reminder Fragment - This is the fragment where the user can interact with reminders and edit their properties.
 * Two ways to enter - create a new reminder, and clicking on a current reminder to edit it.
 *
 */
class EditReminderFrag : Fragment(), TimePickerFragment.OnTimePickerSelectedListener, DatePickerFragment.OnDatePickerSelectedListener,
    RecurTypeDialogFrag.OnRecurTypeSelectedListener, ReminderStatusDialog.OnReminderStatusSelectListener,
    DaysDialogPicker.SetOnClickDaysPickerListener, MonthsDialogPicker.SetOnClickMonthsPickerListener, ReminderEngine.VolleyCallback {

    private var recurTypeInt = 0
    private var reminderStatusInt = 0
    private val days = mutableListOf<DayOfWeek>()
    private val months = mutableListOf<Month>()
    private val completeTime = LocalTime.now()
    private val completeDate = LocalDate.now()
    private val startDate = LocalDate.now()
    private val endDate = LocalDate.now()
    private val notificationTime = LocalTime.now()
    private var isUpdateBool = false
    private var reminderIDarg = 0

    /**
     * On Create View - this is where edit text listeners are set.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        Timber.i("onCreateView method called")
        val binder: FragmentEditReminderBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_edit_reminder,container,false)
        val reminderArgs = EditReminderFragArgs.fromBundle(arguments!!).reminderId
        reminderIDarg = reminderArgs

        binder.bottomAppBar.setNavigationOnClickListener { view: View ->
            Navigation.findNavController(view).navigate(R.id.action_editReminderFrag_to_remindersMain)
        }

        binder.bAccept.setOnClickListener {
            if(!errorChecksOnAccept()) {
                ReminderEngine.setCreateReminderListener(this)
                createReminder(activity!!)

            }
            else {
                Toast.makeText(activity, "Review Errors and correct", Toast.LENGTH_SHORT).show()
            }
            //Navigation.findNavController(view).navigate(R.id.action_editReminderFrag_to_remindersMain)
        }

        binder.bCancel.setOnClickListener { view: View ->
            //Navigation.findNavController(view).navigate(R.id.action_editReminderFrag_to_remindersMain)
            Navigation.findNavController(view).popBackStack()

        }
        binder.etReminderTitle.setOnFocusChangeListener {_,_ ->
            if (tilReminderTitle.isErrorEnabled) {
                tilReminderTitle.isErrorEnabled = false
            }
        }
        binder.etReminderDescription.setOnFocusChangeListener { _,_ ->
            if (tilReminderDesc.isErrorEnabled) {
                tilReminderDesc.isErrorEnabled = false
            }
        }

        binder.etCompleteDate.setOnClickListener {
            if (tilCompleteDate.isErrorEnabled){
                tilCompleteDate.isErrorEnabled = false
            }
            showDatePickerFragment(binder.etCompleteDate,completeDate)

        }

        binder.etCompleteTime.setOnClickListener {
            if (tilCompleteTime.isErrorEnabled)
                tilCompleteTime.isErrorEnabled = false
            showTimePickerFragment(binder.etCompleteTime,completeTime)
        }


        binder.etReminderStatus.setOnClickListener {
            if (tilReminderStatus.isErrorEnabled)
                tilReminderStatus.isErrorEnabled = false
            showReminderStatusPicker(binder.etReminderStatus)
        }

        binder.etSetRecurType.setOnClickListener {
            if (tilSetRecurType.isErrorEnabled)
                tilSetRecurType.isErrorEnabled = false
            showRecurType()
        }

        binder.etStartDate.setOnClickListener {
            if (tilStartDate.isErrorEnabled)
                tilStartDate.isErrorEnabled = false
            showDatePickerFragment(binder.etStartDate,startDate)
        }

        binder.etEndDate.setOnClickListener {
            if (tilEndDate.isErrorEnabled)
                tilEndDate.isErrorEnabled = false
            showDatePickerFragment(binder.etEndDate, endDate)
        }

        binder.etNotificationTime.setOnClickListener {
            if (tilNotificationTime.isErrorEnabled)
                tilNotificationTime.isErrorEnabled = false
            showTimePickerFragment(binder.etNotificationTime, notificationTime)
        }

        binder.etDays.setOnClickListener {
            if (tilDays.isErrorEnabled)
                tilDays.isErrorEnabled = false
            showDaysPicker(binder.etDays)
        }

        binder.etMonths.setOnClickListener {
            if (tilMonths.isErrorEnabled)
                tilMonths.isErrorEnabled = false
            showMonthsPicker(binder.etMonths)
        }

        binder.etDayOfMonth.setOnFocusChangeListener { _,_ ->
            if (tilDayOfMonth.isErrorEnabled)
                tilDayOfMonth.isErrorEnabled = false
        }

        binder.etWeekOfMonth.setOnFocusChangeListener { _,_ ->
            if(tilWeekOfMonth.isErrorEnabled)
                tilWeekOfMonth.isErrorEnabled = false
        }

        binder.etFrequency.setOnFocusChangeListener { _,_ ->
            if (tilFrequency.isErrorEnabled)
                tilFrequency.isErrorEnabled = false
        }

        return binder.root
    }

    override fun onResume() {
        super.onResume()
        if (reminderIDarg > 0){
            isUpdateBool = true
            setReminderValues()
        }
        else isUpdateBool = false
    }

    /**
     * Launches the Recur Type dialog selector
     *
     */
    private fun showRecurType(){
        Timber.d("popUpRecurType")
        val rdf = RecurTypeDialogFrag(recurTypeInt)
        rdf.show(activity!!.supportFragmentManager,"RecurTypeDialogFrag")
        rdf.setRecurTypePickerListener(this)
    }

    /**
     * This method sets the text fields when a reminder is clicked on from the ReminderMain Frag
     *
     */
    private fun setReminderValues(){

        var noneRecur = false
        var dailyRecur = false
        var weeklyRecur = false
        var monthlyRecur = false
        var yearlyRecur = false

        for (x in ReminderEngine.reminderListFull){
            if (x.ReminderID != reminderIDarg){
                continue
            }
            else{
                Timber.d(x.ReminderTitle)
                etReminderTitle.setText(x.ReminderTitle)
                etReminderDescription.setText(x.ReminderDescription)
                if (x.ReminderRecurType == 0) {
                    etCompleteDate.setText(x.CompletionDate.format(DateTimeFormatter.ofPattern("M/dd/yyyy")))
                    etCompleteTime.setText(x.CompletionTime.format(DateTimeFormatter.ofPattern("hh:mm a")))
                }
                val reminderStatusString = arrayOf<String>("Inactive","Active","Completed")
                etReminderStatus.setText(reminderStatusString[x.ReminderStatus])
                reminderStatusInt = x.ReminderStatus
                recurTypeInt = x.ReminderRecurType
                if (x.ReminderRecurType != 0) {
                    val reminderRecurType = arrayOf<String>("None","Daily","Weekly","Monthly","Yearly")
                    etSetRecurType.setText(reminderRecurType[x.ReminderRecurType])
                    when (x.ReminderRecurType) {
                        0 -> noneRecur = true
                        1 -> dailyRecur = true
                        2 -> weeklyRecur = true
                        3 -> monthlyRecur = true
                        4 -> yearlyRecur = true
                        else -> noneRecur = true
                    }
                    etStartDate.setText(x.RecurStartDate.format(DateTimeFormatter.ofPattern("M/dd/yyyy")))
                    etEndDate.setText(x.RecurEndDate.format(DateTimeFormatter.ofPattern("M/dd/yyyy")))
                    etNotificationTime.setText(x.RecurNoticeTime.format(DateTimeFormatter.ofPattern("hh:mm a")))
                    etFrequency.setText(x.FrequencyR.toString())
                    if (x.DateOfMonth != 0)
                        etDayOfMonth.setText(x.DateOfMonth.toString())
                    if (x.RecurDays.isNotEmpty()) {
                        onDaysDialogPickerEnd(x.RecurDays,etDays)
                    }
                    if (x.WeekOfMonth != 0)
                        etWeekOfMonth.setText(x.WeekOfMonth.toString())
                    if (x.RecurMonths.isNotEmpty())
                        onMonthsDialogPickerEnd(x.RecurMonths,etMonths)

                    onRecurPickerEnd(dailyRecur, weeklyRecur, monthlyRecur, yearlyRecur, noneRecur)
                }
            }
        }
    }

    /**
     * Triggered from the accept button. This goes through the process of creating a reminder,
     * using the ReminderEngine
     *
     * @param context
     */
    private fun createReminder(context: Context) {
        val sr = ReminderEngine.singleReminderInstance
        ReminderEngine.clear()
        if (isUpdateBool)
            sr.ReminderID = reminderIDarg
        Timber.i("sendReminderToMongo method called")
        sr.ReminderTitle = etReminderTitle.text.toString()
        sr.ReminderDescription = etReminderDescription.text.toString()
        sr.ReminderStatus = reminderStatusInt
        sr.ReminderRecurType = recurTypeInt

        if (recurTypeInt == 0) {
            sr.CompletionDate = LocalDate.parse(etCompleteDate.text.toString(),
                DateTimeFormatter.ofPattern("M/dd/yyyy"))
            sr.CompletionTime = LocalTime.parse(etCompleteTime.text.toString(),
                DateTimeFormatter.ofPattern("hh:mm a"))
        }
        else{
            sr.RecurStartDate = LocalDate.parse(etStartDate.text.toString(),
                DateTimeFormatter.ofPattern("M/dd/yyyy"))
            sr.RecurEndDate = LocalDate.parse(etEndDate.text.toString(),
                DateTimeFormatter.ofPattern("M/dd/yyyy"))
            sr.RecurNoticeTime = LocalTime.parse(etNotificationTime.text.toString(),
                DateTimeFormatter.ofPattern("hh:mm a"))
            sr.FrequencyR = etFrequency.text.toString().toInt()
            if (etDays.text.toString() != ""){
                val stringSplit = etDays.text.toString().split(", ")
                val dayList = mutableListOf<DayOfWeek>()
                for (x in stringSplit) {
                    for (y in DayOfWeek.values()) {
                        if (x.toLowerCase(Locale.getDefault()) == y.toString().toLowerCase(Locale.getDefault()).substring(0,3)) dayList.add(y)
                    }
                }
                sr.RecurDays.addAll(0,dayList)
            }
            if (etMonths.text.toString() != ""){
                val stringSplit = etMonths.text.toString().split(", ")
                val monthList = mutableListOf<Month>()
                for (x in stringSplit) {
                    for (y in Month.values()) {
                        if (x.toLowerCase(Locale.getDefault()) == y.toString().toLowerCase(Locale.getDefault()).substring(0,3)) monthList.add(y)
                    }
                }
                sr.RecurMonths.addAll(0,monthList)
            }
            if (etDayOfMonth.text.toString() != ""){
                sr.DateOfMonth = etDayOfMonth.text.toString().toInt()
            }
            if (etWeekOfMonth.text.toString() != ""){
                sr.WeekOfMonth = etWeekOfMonth.text.toString().toInt()
            }
        }
        if (isUpdateBool) ReminderEngine.updateReminder(context)
        else ReminderEngine.createNewReminder(context)
    }

    /**
     * Error checking in the edit reminder fragment. In general this triggers on the accept button.
     * Listeners for the edit text focus will remove the errors when the edit text is changed
     * These errors will check again every time the accept button is pressed, and return true if there
     * is an error.
     *
     * Basic error checking.
     * Looking for Title. If recur type is 0 will also look for complete date.
     * If recur type is anything other than 0 will check for start/end date. Will also make sure appropriate
     * fields are filled out based on recur type.
     *
     * @return
     */
    private fun errorChecksOnAccept(): Boolean{
        var errorReport = false

        if (etReminderTitle.text.isEmpty()) {
            tilReminderTitle.error = "Please add a Reminder Title"
            errorReport = true
        }

        if (recurTypeInt == 0 && etCompleteDate.text.isEmpty()) {
            tilCompleteDate.error = "Please set a Complete Date"
            errorReport = true
        }
        if (recurTypeInt == 0 && etCompleteTime.text.isEmpty()) {
            tilCompleteTime.error = "Please set a Complete Time"
            errorReport = true
        }

        if (recurTypeInt != 0){
            if (etStartDate.text.isNotEmpty() && etNotificationTime.text.isNotEmpty()){
                val startTDate = LocalDate.parse(etStartDate.text.toString(), DateTimeFormatter.ofPattern("M/dd/yyyy"))
                if (startTDate.isEqual(LocalDate.now()) && LocalTime.parse(etNotificationTime.text.toString(),
                        DateTimeFormatter.ofPattern("hh:mm a")).isBefore(LocalTime.now())) {
                    tilStartDate.error = "StartDate / Notification time already passed"
                    tilNotificationTime.error ="Start Date / Notification time already passed"
                    errorReport = true
                }
            }
            if (etEndDate.text.isNotEmpty() && etStartDate.text.isNotEmpty()){
                if (LocalDate.parse(etStartDate.text.toString(), DateTimeFormatter.ofPattern("M/dd/yyyy")).isAfter(
                        LocalDate.parse(etEndDate.text.toString(), DateTimeFormatter.ofPattern("M/dd/yyyy")))){
                    tilEndDate.error = "End Date needs to be after Start Date"
                    errorReport = true
                }
            }
            if (etDayOfMonth.text.isNotEmpty() && etDays.text.isNotEmpty()){
                tilDayOfMonth.error = "Can only have Day of Month or Days not both"
                tilDays.error = "Can only have Day of Month or Days not both"
                errorReport = true
            }
            if (etDayOfMonth.text.isNotEmpty() && etWeekOfMonth.text.isNotEmpty()){
                tilDayOfMonth.error  = "Cannot have Week of Month with a Day of Month"
                tilWeekOfMonth.error = "Cannot have Week of Month with a Day of Month"
                errorReport = true
            }
            if (etStartDate.text.isEmpty()) {
                tilStartDate.error = "Please set an End Date"
                errorReport = true
            }

            if (recurTypeInt == 2 && etDays.text.isEmpty() ) {
                tilDays.error = "Please select the days for recurring reminder"
                errorReport = true
            }
            if (recurTypeInt == 3 && etDays.text.isEmpty() && etDayOfMonth.text.isEmpty()){
                tilDays.error = "Please select either day or day of month"
                tilDayOfMonth.error = "Please select either day or day of month"
                errorReport = true
            }
            if (recurTypeInt == 3 && etDays.text.isNotEmpty() && etDayOfMonth.text.isNotEmpty()){
                tilDays.error = "Can only select days or day of month"
                tilDayOfMonth.error = "Can only select days or day of month"
                errorReport = true
            }
            if(recurTypeInt == 4 && etMonths.text.isEmpty()) {
                tilMonths.error = "Please select month/months"
                errorReport = true
            }
            if (recurTypeInt == 4 && etDayOfMonth.text.isEmpty()){
                tilDayOfMonth.error = "Please select a day of the month"
                errorReport = true
            }

        }

        return errorReport
    }

    /**
     * Shows the time dialog fragment for each time field
     *
     * @param editText
     * @param c
     */
    private fun showTimePickerFragment(editText: EditText, c:LocalTime){
        Timber.i("showTimePickerFragment method called")
        val tpf = TimePickerFragment(c)
        tpf.show(activity!!.supportFragmentManager,"timePicker")
        tpf.setTimePickerSelectedListener(this,editText)
    }

    /**
     * Shows the date dialog fragment for each date field
     *
     * @param editText
     * @param c
     */
    private fun showDatePickerFragment(editText: EditText, c:LocalDate) {
        Timber.i("showDatePickerFragment method called")
        val dpf = DatePickerFragment(c)

        dpf.show(activity!!.supportFragmentManager, "datePicker")
        dpf.setDatePickerSelectedListener(this, editText)
    }

    /**
     * Shows the reminder status dialog fragment
     *
     * @param editText
     */
    private fun showReminderStatusPicker(editText: EditText) {
        Timber.i ("showReminderStatusPicker method called")
        val rsp = ReminderStatusDialog(reminderStatusInt)

        rsp.show(activity!!.supportFragmentManager,rsp.tag)
        rsp.setOnReminderStatusListener(this,editText)
    }

    /**
     * Shows the day dialog fragment
     *
     * @param editText
     */
    private fun showDaysPicker(editText: EditText){
        Timber.i("showDaysPicker method called")
        val ddp = DaysDialogPicker(days)

        ddp.show(activity!!.supportFragmentManager,ddp.tag)
        ddp.setOnClickDaysPickerListener(this,editText)
    }

    /**
     * Shows the month dialog fragment
     *
     * @param editText
     */
    private fun showMonthsPicker(editText: EditText) {
        Timber.i("showMonthsPicker method called")
        val mdp = MonthsDialogPicker(months)

        mdp.show(activity!!.supportFragmentManager,mdp.tag)
        mdp.setOnClickMonthsPickerListener(this,editText)

    }

    /**
     * This is the interface method that is run when the time picker is closed.
     * Fills out appropriate edit text and time property so we can pass time back and forth
     * between time picker and editreminder frag.
     *
     * @param hourOfDay
     * @param min
     * @param etField
     * @param c
     */
    override fun onTimePickerEnd(hourOfDay: Int, min: Int, etField: EditText,c: LocalTime) {
        Timber.i("onTimePickerEnd method called")
        etField.setText(
            getString(
                R.string.erfPMHourMin,
                c.format(DateTimeFormatter.ofPattern("hh:mm a"))
            )
        )
    }

    /**
     * This is the method that is run when the Date dialog fragment is closed.
     * fills out appropriate EditText and date property.
     *
     * @param year
     * @param month
     * @param dayOfMonth
     * @param editText
     * @param c
     */
    override fun onDatePickerEnd(editText: EditText,c: LocalDate) {
        Timber.i("onDatePickerEnd method called")
        val x = Calendar.getInstance()
        x.set(c.year,c.monthValue-1,c.dayOfMonth)
        editText.setText(getString(R.string.erfDateFormat,x))
    }

    /**
     * This is the interface method that is run when the Recur type dialog is closed.
     * Sets the recurTypeInt and fills out etRecurType text.
     *
     * @param daily
     * @param weekly
     * @param Monthly
     * @param yearly
     * @param none
     */
    override fun onRecurPickerEnd(
        daily: Boolean,
        weekly: Boolean,
        Monthly: Boolean,
        yearly: Boolean,
        none: Boolean
    ) {

        if (daily) {
            etSetRecurType.setText("Daily")
            recurTypeInt = 1
            tilStartDate.visibility = View.VISIBLE
            tilEndDate.visibility = View.VISIBLE
            tilNotificationTime.visibility = View.VISIBLE
            tilFrequency.visibility = View.VISIBLE
            tilCompleteDate.visibility = View.GONE
            tilCompleteTime.visibility = View.GONE
            tilDayOfMonth.visibility = View.GONE
            tilDays.visibility = View.GONE
            tilMonths.visibility = View.GONE
            tilWeekOfMonth.visibility = View.GONE
            editfragDivider.visibility = View.VISIBLE
        }
        else if (weekly) {
            etSetRecurType.setText("Weekly")
            recurTypeInt = 2
            tilStartDate.visibility = View.VISIBLE
            tilEndDate.visibility = View.VISIBLE
            tilNotificationTime.visibility = View.VISIBLE
            tilFrequency.visibility = View.VISIBLE
            tilCompleteDate.visibility = View.GONE
            tilCompleteTime.visibility = View.GONE
            tilDayOfMonth.visibility = View.GONE
            tilDays.visibility = View.VISIBLE
            tilMonths.visibility = View.GONE
            tilWeekOfMonth.visibility = View.GONE
            editfragDivider.visibility = View.VISIBLE

        }
        else if (Monthly) {
            etSetRecurType.setText("Monthly")
            recurTypeInt = 3
            tilStartDate.visibility = View.VISIBLE
            tilEndDate.visibility = View.VISIBLE
            tilNotificationTime.visibility = View.VISIBLE
            tilFrequency.visibility = View.VISIBLE
            tilCompleteDate.visibility = View.GONE
            tilCompleteTime.visibility = View.GONE
            tilDayOfMonth.visibility = View.VISIBLE
            tilDays.visibility = View.VISIBLE
            tilMonths.visibility = View.GONE
            tilWeekOfMonth.visibility = View.VISIBLE
            editfragDivider.visibility = View.VISIBLE
        }
        else if (yearly) {
            etSetRecurType.setText("Yearly")
            recurTypeInt = 4
            tilStartDate.visibility = View.VISIBLE
            tilEndDate.visibility = View.VISIBLE
            tilNotificationTime.visibility = View.VISIBLE
            tilFrequency.visibility = View.VISIBLE
            tilCompleteDate.visibility = View.GONE
            tilCompleteTime.visibility = View.GONE
            tilDayOfMonth.visibility = View.VISIBLE
            tilDays.visibility = View.GONE
            tilMonths.visibility = View.VISIBLE
            tilWeekOfMonth.visibility = View.GONE
            editfragDivider.visibility = View.VISIBLE
        }
        else {
            etSetRecurType.setText("None")
            recurTypeInt = 0
            tilStartDate.visibility = View.GONE
            tilEndDate.visibility = View.GONE
            tilNotificationTime.visibility = View.GONE
            tilFrequency.visibility = View.GONE
            tilCompleteDate.visibility = View.VISIBLE
            tilCompleteTime.visibility = View.VISIBLE
            tilDayOfMonth.visibility = View.GONE
            tilDays.visibility = View.GONE
            tilMonths.visibility = View.GONE
            tilWeekOfMonth.visibility = View.GONE
            editfragDivider.visibility = View.GONE
        }

    }

    /**
     * This is the method that is run when the reminder status dialog is closed.
     * Fills out reminderStatusInt property and fills out etReminderStatus
     *
     * @param string
     * @param etField
     */
    override fun onReminderStatusPickerEnd(string: String, etField: EditText) {
        etField.setText(string)
        reminderStatusInt = when (string) {
            "Active" -> 1
            "Inactive" -> 0
            "Complete" -> 2
            else -> 0
        }

    }

    /**
     * This is the method that is run when the days dialog picker is closed.
     * Fills out et Days with the appropriate days as comma delimited 'Mon,Tue,Wed'.... etc
     * Also fills out days property.
     *
     * @param days
     * @param editText
     */
    override fun onDaysDialogPickerEnd(days: MutableList<DayOfWeek>, editText: EditText) {
        var etString: String = ""
        this.days.clear()
        this.days.addAll(0,days)
        if (days.size == 0) {
            editText.setText("None Selected")
            return
        }
        for (x in days) {
            etString = if (x != days[days.size-1]) "$etString${x.toString().substring(0,3)}, "
            else "$etString${x.toString().substring(0,3)}"
       }
        editText.setText(etString)
    }

    /**
     * This method is run when Month dialog picker is closed. Fills out month dialog text with,
     * comma delimited list - 'Jan,Feb,Sep'.... also fills out months property.
     *
     * @param Months
     * @param editText
     */
    override fun onMonthsDialogPickerEnd(Months: MutableList<Month>, editText: EditText) {
        var etString: String = ""
        months.clear()
        months.addAll(0,Months)
        if (Months.size == 0) {
            editText.setText("None Selected")
            return
        }
        for (x in Months) {
            etString = if (x != Months[Months.size-1]) "${etString}${x.toString().substring(0,3)}, "
            else "$etString${x.toString().substring(0,3)}"
        }
        editText.setText(etString)
    }

    override fun onCreateReminderResponse(result: Boolean) {
        if (!isUpdateBool) {
            if (result)
                Toast.makeText(activity, "Reminder Created!", Toast.LENGTH_SHORT).show()
            else
                Toast.makeText(activity, "Something Went Wrong!", Toast.LENGTH_SHORT).show()
        }
        else {
            if (result)
                Toast.makeText(activity, "Reminder Updated!", Toast.LENGTH_SHORT).show()
            else
                Toast.makeText(activity, "Something Went Wrong!", Toast.LENGTH_SHORT).show()
        }

        findNavController().popBackStack()
    }

    override fun onSuccessResponse(result: String) {
        //not in use currently
    }
}
