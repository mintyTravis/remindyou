package iris.technology.remindingyou


import android.content.Context
import androidx.core.app.NotificationCompat
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyLog
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONStringer
import org.json.JSONTokener
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalTime
import org.threeten.bp.Month
import timber.log.Timber
import java.io.File
import java.nio.charset.Charset
import kotlin.math.abs

/**
 * This is the ReminderEngine object (class). There is only meant to be one instance in the lifespan of the app.
 * It controls all the REST calls to and from the server and passes them back to fragments.
 * It also handles storing and constructing Reminders for appropriate display in fragments.
 *
 * REST calls are done primarily using JSON format
 *
 * @property reminderJSONObjectList - used to store multiple JSON object reminders
 * @property singleReminderInstance - used to modify single Reminder
 * @property callback - used as an interface back to fragment
 * @property reminderListToday - used as a dataset of Today's reminders for ReminderMain Frag
 * @property reminderListUpcoming - used as a dataset for Upcoming reminders for ReminderMain Frag
 * @property reminderListRecurring - used as a dataset of all 'Parent' recurring reminders for ReminderMain Frag
 * @property reminderListShared - used as a dataset of all shared reminders for ReminderMain Frag
 * @property rUserID - stores userID
 * @property userLogin - stores current user login from google
 * @property volleyQueue - sets the volley queue with context.
 *
 *
 */
object ReminderEngine {
    private val reminderJSONObjectList = JSONArray()
    val singleReminderInstance = Reminder()
    private lateinit var callback: VolleyCallback
    val reminderListFull: MutableList<Reminder> = mutableListOf()
    val reminderListToday: MutableList<Reminder> = mutableListOf()
    val reminderListUpcoming: MutableList<Reminder> = mutableListOf()
    val reminderListRecurring: MutableList<Reminder> = mutableListOf()
    val reminderListShared: MutableList<Reminder> = mutableListOf()
    private var rUserID: Int = -1
    private lateinit var volleyQueue: RequestQueue
    private val CHANNEL_ID: String = "RemindYou"
    private var localFileSyncBool: Boolean = false

    /**
     * Sets the volleyQueue with current fragment context.
     * TODO: Use this for each fragment to set context for volleys
     *
     * @param context
     */
    fun setVolleyRequestQueue(context: Context) {
        volleyQueue = Volley.newRequestQueue(context)
    }

    /**
     * Method that should be called when creating a new reminder
     *
     * @param context
     */
    fun createNewReminder(context: Context) {

        when (singleReminderInstance.ReminderRecurType) {
            1 -> setDailyRecurReminders(singleReminderInstance, true)
            2 -> setWeeklyRecurReminders(singleReminderInstance, true)
            3 -> setMonthlyRecurReminders(singleReminderInstance, true)
            4 -> setYearlyRecurReminders(singleReminderInstance, true)
        }
        singleReminderInstance.OwnerID = UserEngine.returnUserID()
        toJSONObject()
        volleyToMongoPostOne(context)
    }

    fun updateReminder(context: Context) {
        when (singleReminderInstance.ReminderRecurType) {
            1 -> setDailyRecurReminders(singleReminderInstance, true)
            2 -> setWeeklyRecurReminders(singleReminderInstance, true)
            3 -> setMonthlyRecurReminders(singleReminderInstance, true)
            4 -> setYearlyRecurReminders(singleReminderInstance, true)
        }
        singleReminderInstance.OwnerID = UserEngine.returnUserID()
        toJSONObject()
        volleyToMongoPostOne(context)
    }

    fun completeReminder(context: Context, reminder: Reminder) {
        singleReminderInstance.setAs(reminder)
        singleReminderInstance.ReminderStatus = Reminder.REMINDER_COMPLETE
        toJSONObject()
        volleyToMongoPostOne(context)
    }

    fun cleanReminder(context: Context, r: Reminder){

        for ((index,x) in reminderListToday.withIndex()) {
            if (x.ReminderID == r.ReminderID){
                reminderListToday.removeAt(index)
                break
            }
        }
        for ((index,x) in reminderListUpcoming.withIndex()) {
            if (x.ReminderID == r.ReminderID) {
                reminderListUpcoming.removeAt(index)
                break
            }
        }
        for ((index,x) in reminderListRecurring.withIndex()){
            if (x.ReminderID == r.ReminderID) {
                reminderListRecurring.removeAt(index)
                break
            }
        }

        toJSONObject()
        volleyToMongoPostOne(context)
    }

    /**
     * Method called to retrieve all users reminders from database
     *
     * @param context
     * @param callback
     */
    fun getAllReminders(context: Context, callback: VolleyCallback) {
        if (singleReminderInstance.OwnerID == 0) singleReminderInstance.OwnerID = UserEngine.returnUserID()
        volleyToMongoGetAll(context, callback)
    }

    private fun syncLocalFile(context: Context,reminders: JSONArray){
        localFileSyncBool = false
        var fileString: String
        val rFile = File(context.filesDir,"reminderFile")
        if (rFile.exists()) {
            fileString = rFile.readText(Charset.defaultCharset())
            Timber.i("remind_you - $fileString")
            Timber.i("remind_you - $reminders")
            if (rFile.length() != 0.toLong()) {
                if (fileString == reminders.toString())
                    localFileSyncBool = true
                else {
                    rFile.writeText(reminders.toString())
                    localFileSyncBool = true
                }
            }
            else {
                rFile.writeText(reminders.toString())
                localFileSyncBool = true
            }
        }
        else
            rFile.createNewFile()
        Timber.i(rFile.absolutePath)
    }

    /**
     * Clears information from current singleReminderInstance
     *
     */
    fun clear() {
        singleReminderInstance.ReminderID = 0
        singleReminderInstance.OwnerID = 0
        singleReminderInstance.UserIDs = intArrayOf(0)
        singleReminderInstance.GroupIDs = intArrayOf(0)
        singleReminderInstance.ReminderTitle = ""
        singleReminderInstance.ReminderDescription = ""
        singleReminderInstance.ReminderStatus = 0
        singleReminderInstance.ReminderTag = ""
        singleReminderInstance.ReminderLocation = ""
        singleReminderInstance.CreationDate = LocalDate.now()
        singleReminderInstance.CreationTime = LocalTime.now()
        singleReminderInstance.CompletionDate = LocalDate.of(2000, 1, 1)
        singleReminderInstance.CompletionTime = LocalTime.of(12, 0)
        singleReminderInstance.ReminderRecurType = 0
        singleReminderInstance._id = ""
        singleReminderInstance.RecurStartDate = LocalDate.of(2000, 1, 1)
        singleReminderInstance.RecurEndDate = LocalDate.of(2000, 1, 1)
        singleReminderInstance.FrequencyR = 0
        singleReminderInstance.RecurNoticeTime = LocalTime.of(12, 0)
        singleReminderInstance.RecurDays.clear()
        singleReminderInstance.RecurMonths.clear()
        singleReminderInstance.DateOfMonth = 0
        singleReminderInstance.WeekOfMonth = 0
        singleReminderInstance.RecurNextReminder = LocalDate.of(2000, 1, 1)
        singleReminderInstance.recurReminderList.clear()
    }

    /**
     * Converts singleREminderInstance to JSON object.
     *
     */
    fun toJSONObject() {
        val arrayUserIDs = JSONArray()
        val arrayGroupIDs = JSONArray()
        val arrayDays = JSONArray()
        val arrayMonths = JSONArray()
        val arrayRecurList = JSONArray()

        singleReminderInstance.reminderJSONObject.put(
            "ReminderID",
            singleReminderInstance.ReminderID
        )
        singleReminderInstance.reminderJSONObject.put("OwnerID", singleReminderInstance.OwnerID)
        for (x in singleReminderInstance.UserIDs) {
            arrayUserIDs.put(x)
        }
        singleReminderInstance.reminderJSONObject.put("UserIDs", arrayUserIDs)
        for (x in singleReminderInstance.GroupIDs) {
            arrayGroupIDs.put(x)
        }
        singleReminderInstance.reminderJSONObject.put("GroupIDs", arrayGroupIDs)
        singleReminderInstance.reminderJSONObject.put(
            "ReminderTitle",
            singleReminderInstance.ReminderTitle
        )
        singleReminderInstance.reminderJSONObject.put(
            "ReminderDescription",
            singleReminderInstance.ReminderDescription
        )
        singleReminderInstance.reminderJSONObject.put(
            "ReminderStatus",
            singleReminderInstance.ReminderStatus
        )
        singleReminderInstance.reminderJSONObject.put(
            "ReminderTag",
            singleReminderInstance.ReminderTag
        )
        singleReminderInstance.reminderJSONObject.put(
            "ReminderLocation",
            singleReminderInstance.ReminderLocation
        )
        singleReminderInstance.reminderJSONObject.put(
            "CreationDate",
            singleReminderInstance.CreationDate
        )
        singleReminderInstance.reminderJSONObject.put(
            "CreationTime",
            singleReminderInstance.CreationTime
        )
        singleReminderInstance.reminderJSONObject.put(
            "CompletionDate",
            singleReminderInstance.CompletionDate
        )
        singleReminderInstance.reminderJSONObject.put(
            "CompletionTime",
            singleReminderInstance.CompletionTime
        )
        singleReminderInstance.reminderJSONObject.put(
            "ReminderRecurType",
            singleReminderInstance.ReminderRecurType
        )
        singleReminderInstance.reminderJSONObject.put(
            "FrequencyR",
            singleReminderInstance.FrequencyR
        )
        singleReminderInstance.reminderJSONObject.put(
            "RecurStartDate",
            singleReminderInstance.RecurStartDate
        )
        singleReminderInstance.reminderJSONObject.put(
            "RecurEndDate",
            singleReminderInstance.RecurEndDate
        )
        singleReminderInstance.reminderJSONObject.put(
            "RecurNoticeTime",
            singleReminderInstance.RecurNoticeTime
        )
        for (x in singleReminderInstance.RecurDays) {
            arrayDays.put(x)
        }
        singleReminderInstance.reminderJSONObject.put("RecurDays", arrayDays)
        for (x in singleReminderInstance.RecurMonths) {
            arrayMonths.put(x)
        }
        singleReminderInstance.reminderJSONObject.put("RecurMonths", arrayMonths)
        singleReminderInstance.reminderJSONObject.put(
            "DateOfMonth",
            singleReminderInstance.DateOfMonth
        )
        singleReminderInstance.reminderJSONObject.put(
            "WeekOfMonth",
            singleReminderInstance.WeekOfMonth
        )
        singleReminderInstance.reminderJSONObject.put(
            "RecurNextReminder",
            singleReminderInstance.RecurNextReminder
        )
        for (x in singleReminderInstance.recurReminderList) {
            arrayRecurList.put(x)
        }
        singleReminderInstance.reminderJSONObject.put("recurReminderList", arrayRecurList)
        singleReminderInstance.reminderJSONObject.put("GoogleToken",UserEngine.returnGoogleToken())
        Timber.d(singleReminderInstance.reminderJSONObject.toString())
    }

    /**
     * Adds reminder to MongoDB through the Google Apps Engine
     *
     * @param context
     */
    private fun volleyToMongoPostOne(context: Context) {
        Timber.i("volleyToMongoPostOne method called")
        val queue = Volley.newRequestQueue(context)
        val url = "http://remind-you-1.appspot.com/reminders/add/"
        // Test Link
        //val url = "http://192.168.1.174:5050/reminders/add/"
        val reminderSend = JsonObjectRequest(
            Request.Method.POST,
            url,
            singleReminderInstance.reminderJSONObject,
            Response.Listener { response ->
                for (x in response.keys()) {
                    if (x == "Error") {
                        callback.onCreateReminderResponse(false)
                        return@Listener
                    }
                }
                for (x in singleReminderInstance.reminderJSONObject.keys()) {
                    Timber.d(x)
                    if (!response.has(x)) continue
                    if (singleReminderInstance.reminderJSONObject.get(x) == response.get(x)) continue
                    else singleReminderInstance.reminderJSONObject.put(x, response.get(x))
                }
                if (response.has("_id")) {
                    singleReminderInstance.reminderJSONObject.put("_id", response.get("_id"))
                    singleReminderInstance._id = response.getString("_id")
                }
                callback.onCreateReminderResponse(true)

            },
            Response.ErrorListener { error ->
                Timber.i(error.toString())
            })
        queue.add(reminderSend)
    }

    /**
     * Retrieves the user reminders from MongoDB using Google Apps Engine
     * Sets up a listener with fragment to retrieve reminders once response comes back
     *
     * @param context
     * @param callback - used for listener response
     */
    private fun volleyToMongoGetAll(context: Context, callback: VolleyCallback) {
        Timber.i("volleyToMongoGetAll method called")
        VolleyLog.DEBUG = true
        this.callback = callback

        val sendArray = JSONArray()
        val createJSON = JSONObject()
        createJSON.put("OwnerID", singleReminderInstance.OwnerID)
        createJSON.put("GoogleToken",UserEngine.returnGoogleToken())
        sendArray.put(createJSON)

        for (x in reminderJSONObjectList.length() downTo 0){
            reminderJSONObjectList.remove(x)
        }
        Timber.i("${reminderJSONObjectList.length()}")
        val queue = Volley.newRequestQueue(context)
        val url = "http://remind-you-1.appspot.com/reminders/fetch/user/"
        // Test Link
        // val url = "http://10.0.1.164:5050/reminders/fetch/user/"
        for (x in 0.until(reminderJSONObjectList.length() - 1)) reminderJSONObjectList.remove(x)
        val remindersGet = JsonArrayRequest(
            Request.Method.POST,
            url,sendArray,
            Response.Listener<JSONArray> { response ->
                for (x in 0.until(response.length())) {
                    reminderJSONObjectList.put(response[x])
                }
                Timber.i(reminderJSONObjectList.toString())
                createReminderList(reminderJSONObjectList.length())
                syncLocalFile(context,response)
                callback.onSuccessResponse("Success")
            },
            Response.ErrorListener { error ->
                Timber.i(error.toString())
            })

        queue.add(remindersGet)
        queue.start()
    }

    /**
     * Used for callback to fragment for volleyToMongoGetAll
     * Returns string result to fragment for Reminder Display
     *
     */
    interface VolleyCallback {
        fun onSuccessResponse(result: String)
        fun onCreateReminderResponse(result: Boolean) {
            return
        }
    }

    fun setCreateReminderListener(callback: VolleyCallback) {
        this.callback = callback
    }

    /**
     * Is called after volleyToMongoGetAll
     * Compiles a mutable list of reminders of type Reminder()
     * Loops through all reminders received from MongoDB
     * Puts them in appropriate list according to the reminder type
     *
     * @param numOfReminders - expected number of reminders recieved back from Mongo
     */
    private fun createReminderList(numOfReminders: Int) {
        Timber.i("createReminderList method called")
        Timber.i("number of objects: $numOfReminders")
        reminderListFull.clear()
        reminderListToday.clear()
        reminderListUpcoming.clear()
        reminderListRecurring.clear()
        reminderListShared.clear()

        for (i in 0.until(numOfReminders)) {
            val r = Reminder()
            r.ReminderID = reminderJSONObjectList.getJSONObject(i).getInt("ReminderID")
            r.OwnerID = reminderJSONObjectList.getJSONObject(i).getInt("OwnerID")
            for (x in 0.until(reminderJSONObjectList.getJSONObject(i).getJSONArray("UserIDs").length() - 1)) {
                r.UserIDs[x] =
                    reminderJSONObjectList.getJSONObject(i).getJSONArray("UserIDs").getInt(x)
            }
            for (x in 0.until(reminderJSONObjectList.getJSONObject(i).getJSONArray("GroupIDs").length() - 1)) {
                r.GroupIDs[x] =
                    reminderJSONObjectList.getJSONObject(i).getJSONArray("GroupIDs").getInt(x)
            }
            r.ReminderTitle = reminderJSONObjectList.getJSONObject(i).getString("ReminderTitle")
            r.ReminderDescription =
                reminderJSONObjectList.getJSONObject(i).getString("ReminderDescription")
            r.ReminderStatus = reminderJSONObjectList.getJSONObject(i).getInt("ReminderStatus")
            r.ReminderTag = reminderJSONObjectList.getJSONObject(i).getString("ReminderTag")
            r.ReminderLocation =
                reminderJSONObjectList.getJSONObject(i).getString("ReminderLocation")
            r.CreationDate =
                LocalDate.parse(reminderJSONObjectList.getJSONObject(i).getString("CreationDate"))
            r.CreationTime =
                LocalTime.parse(reminderJSONObjectList.getJSONObject(i).getString("CreationTime"))
            r.CompletionDate =
                LocalDate.parse(reminderJSONObjectList.getJSONObject(i).getString("CompletionDate"))
            r.CompletionTime =
                LocalTime.parse(reminderJSONObjectList.getJSONObject(i).getString("CompletionTime"))
            r.ReminderRecurType =
                reminderJSONObjectList.getJSONObject(i).getInt("ReminderRecurType")
            r.FrequencyR = reminderJSONObjectList.getJSONObject(i).getInt("FrequencyR")
            r.RecurStartDate =
                LocalDate.parse(reminderJSONObjectList.getJSONObject(i).getString("RecurStartDate"))
            r.RecurEndDate =
                LocalDate.parse(reminderJSONObjectList.getJSONObject(i).getString("RecurEndDate"))
            r.RecurNoticeTime =
                LocalTime.parse(reminderJSONObjectList.getJSONObject(i).getString("RecurNoticeTime"))
            for (x in 0.until(reminderJSONObjectList.getJSONObject(i).getJSONArray("RecurDays").length())) {
                r.RecurDays.add(
                    DayOfWeek.valueOf(
                        reminderJSONObjectList.getJSONObject(i).getJSONArray(
                            "RecurDays"
                        ).getString(x)
                    )
                )
            }
            for (x in 0.until(reminderJSONObjectList.getJSONObject(i).getJSONArray("RecurMonths").length())) {
                r.RecurMonths.add(
                    Month.valueOf(
                        reminderJSONObjectList.getJSONObject(i).getJSONArray(
                            "RecurMonths"
                        ).getString(x)
                    )
                )
            }
            r.DateOfMonth = reminderJSONObjectList.getJSONObject(i).getInt("DateOfMonth")
            r.WeekOfMonth = reminderJSONObjectList.getJSONObject(i).getInt("WeekOfMonth")

            for (x in 0.until(reminderJSONObjectList.getJSONObject(i).getJSONArray("recurReminderList").length())) {
                r.recurReminderList.add(
                    LocalDate.parse(
                        reminderJSONObjectList.getJSONObject(i).getJSONArray(
                            "recurReminderList"
                        ).getString(x)
                    )
                )
            }
            r.RecurNextReminder = LocalDate.parse(reminderJSONObjectList.getJSONObject(i).getString("RecurNextReminder"))
            reminderListFull.add(r)
            if (r.CompletionDate == LocalDate.now() || r.RecurNextReminder == LocalDate.now()) {
                reminderListToday.add(r)
            }
            else if (LocalDate.now().plusDays(30) > r.CompletionDate && LocalDate.now() < r.CompletionDate
                || (r.ReminderRecurType > 0 && LocalDate.now().plusDays(30) > r.RecurNextReminder && LocalDate.now() < r.RecurNextReminder))
                reminderListUpcoming.add(r)
            if (r.ReminderRecurType != 0 && LocalDate.now() < r.RecurEndDate) reminderListRecurring.add(r)
            if (r.UserIDs.contains(0) && r.UserIDs.count() > 1) reminderListShared.add(r)
        }
        if (reminderListRecurring.isNotEmpty()) setAllRecurringReminders(reminderListRecurring)
    }

    fun createSingleReminder(jsonOb: JSONObject) : Reminder{
        val r = Reminder()

        r.ReminderID = jsonOb.getInt("ReminderID")
        r.OwnerID = jsonOb.getInt("OwnerID")
        for (x in 0.until(jsonOb.getJSONArray("UserIDs").length()-1)) {
            r.UserIDs[x] = jsonOb.getJSONArray("UserIDs").getInt(x)
        }
        for (x in 0.until(jsonOb.getJSONArray("GroupIDs").length()-1)){
            r.GroupIDs[x] = jsonOb.getJSONArray("GroupIDs").getInt(x)
        }
        r.ReminderTitle = jsonOb.getString("ReminderTitle")
        r.ReminderDescription = jsonOb.getString("ReminderDescription")
        r.ReminderStatus = jsonOb.getInt("ReminderStatus")
        r.ReminderTag = jsonOb.getString("ReminderTag")
        r.ReminderLocation = jsonOb.getString("ReminderLocation")
        r.CreationDate = LocalDate.parse(jsonOb.getString("CreationDate"))
        r.CreationTime = LocalTime.parse(jsonOb.getString("CreationTime"))
        r.CompletionDate = LocalDate.parse(jsonOb.getString("CompletionDate"))
        r.CompletionTime = LocalTime.parse(jsonOb.getString("CompletionTime"))
        r.ReminderRecurType = jsonOb.getInt("ReminderRecurType")
        if (r.ReminderRecurType > 0){
            r.RecurStartDate = LocalDate.parse(jsonOb.getString("RecurStartDate"))
            r.RecurEndDate = LocalDate.parse(jsonOb.getString("RecurEndDate"))
            r.FrequencyR = jsonOb.getInt("FrequencyR")
            r.RecurNoticeTime = LocalTime.parse(jsonOb.getString("RecurNoticeTime"))
            for (x in 0.until(jsonOb.getJSONArray("RecurDays").length()-1)){
                r.RecurDays.add(DayOfWeek.valueOf(jsonOb.getJSONArray("RecurDays").getString(x)))
            }
            for (x in 0.until(jsonOb.getJSONArray("RecurMonths").length()-1)){
                r.RecurMonths.add(Month.valueOf(jsonOb.getJSONArray("RecurMonths").getString(x)))
            }
            r.DateOfMonth = jsonOb.getInt("DateOfMonth")
            r.WeekOfMonth = jsonOb.getInt("WeekOfMonth")
            r.RecurNextReminder = LocalDate.parse(jsonOb.getString("RecurNextReminder"))
        }
        return r
    }

    /**
     * In a new thread this sets the recurring reminders date list for all our recurring reminders
     *
     * @param rl
     */
    private fun setAllRecurringReminders(rl: MutableList<Reminder>) {

        for (x in rl) {
            when (x.ReminderRecurType) {
                1 -> setDailyRecurReminders(x, false)
                2 -> setWeeklyRecurReminders(x, false)
                3 -> setMonthlyRecurReminders(x, false)
                4 -> setYearlyRecurReminders(x, false)
            }
        }
        //Thread(Runnable {
        //TODO: in new thread send updated recurring dates to server
        //}).start()
    }

    /**
     * This function sets the first recurring reminder date and a list of dates of the next reminders
     * up to 365 days in the future
     *
     * @param r - passing singleReminderInstance as r for simple coding in method
     * @param firstReminder -
     */
    private fun setDailyRecurReminders(r: Reminder, firstReminder: Boolean = false) {
        var rOccurrences: Long = 0
        var tempDate: LocalDate = LocalDate.now()

        if (
            r.RecurEndDate.isBefore(LocalDate.now())
            || (r.RecurEndDate == LocalDate.now() && r.RecurNoticeTime.isBefore(LocalTime.now()))
        ) {
            r.ReminderStatus = 2
            return
        }

        if (r.RecurNextReminder.isAfter(LocalDate.now())) {
            return
        }

        if (
            r.RecurNextReminder == LocalDate.now()
            && r.RecurNoticeTime.isAfter(LocalTime.now())
        )   return

        if (firstReminder) {
            r.RecurNextReminder = r.RecurStartDate
            tempDate = r.RecurNextReminder
        } else {
            r.RecurNextReminder = r.recurReminderList[0]
            tempDate = r.RecurNextReminder
            r.recurReminderList.clear()
        }

        while (r.recurReminderList.isEmpty()){
            tempDate = tempDate.plusDays(1)
            if ( tempDate.toEpochDay() - r.RecurNextReminder.toEpochDay() == r.FrequencyR.toLong()) {
                r.recurReminderList.add(tempDate)
            }
        }

        //old code
        /*if (firstReminder) {
            r.RecurNextReminder = r.RecurStartDate
            r.recurReminderList.add(r.RecurNextReminder)

            tempDate = LocalDate.ofEpochDay(r.RecurStartDate.toEpochDay() + 365)
            rOccurrences = if (r.RecurEndDate.isAfter(tempDate))
                ((tempDate.toEpochDay() - r.RecurStartDate.toEpochDay()) / r.FrequencyR)
            else
                ((r.RecurEndDate.toEpochDay() - r.RecurStartDate.toEpochDay()) / r.FrequencyR)

            tempDate = r.RecurStartDate
            for (x in 1.until(rOccurrences)) {
                tempDate = tempDate.plusDays(r.FrequencyR.toLong())
                r.recurReminderList.add(tempDate)
            }
        } else {
            Timber.d("${r.recurReminderList}")
            val iterThrough = mutableListOf<LocalDate>()
            iterThrough.addAll(r.recurReminderList)
            for (x in iterThrough) {
                if (x <= LocalDate.now() || x == r.RecurNextReminder) {
                    tempDate = x.plusYears(1)
                    if (!r.recurReminderList.contains(tempDate))
                        r.recurReminderList.add(tempDate)
                    r.recurReminderList.remove(x)
                    continue
                } else {
                    r.RecurNextReminder = x
                    break
                }
            }
        }*/
        Timber.i("RecurReminderList: ${r.recurReminderList} \nRecurReminderNextDate: ${r.RecurNextReminder}")
    }

    /**
     * Sets Weekly recurring reminders Reminder Dates up to 365 days ahead.
     * TODO: This needs to be tested
     *
     * @param r - recurring reminder
     * @param firstReminder - is it the first time this is being done?
     */
    private fun setWeeklyRecurReminders(r: Reminder, firstReminder: Boolean = false) {
        var rOccurrencesWeeks: Long = 0
        var tempDate: LocalDate = LocalDate.now()
        var RecurNextReminderSet = false

        if (r.RecurEndDate.isBefore(LocalDate.now())
            || (r.RecurEndDate == LocalDate.now() && r.RecurNoticeTime.isBefore(LocalTime.now()))
                ) {
            r.ReminderStatus = 2
            return
        }

        if (r.RecurNextReminder.isAfter(LocalDate.now())) {
            return
        }

        if (
            r.RecurNextReminder == LocalDate.now()
            && r.RecurNoticeTime.isAfter(LocalTime.now())
               )
            return

        if (firstReminder) {
            r.RecurNextReminder = r.RecurStartDate
        }
        else {
            r.RecurNextReminder = r.recurReminderList[0]
            RecurNextReminderSet = true
            r.recurReminderList.clear()
        }

        while (!RecurNextReminderSet) {
            if (r.RecurDays.contains(r.RecurNextReminder.dayOfWeek)) {
                RecurNextReminderSet = true
            }
            else {
                r.RecurNextReminder = r.RecurNextReminder.plusDays(1)
            }
        }

        tempDate = r.RecurNextReminder
        var weekCount = 0
        while (r.recurReminderList.isEmpty()) {
            tempDate = tempDate.plusDays(1)

            if (tempDate.dayOfWeek == DayOfWeek.MONDAY) weekCount++
            if (r.RecurDays.contains(tempDate.dayOfWeek)) {
                if (weekCount == 0) {
                    r.recurReminderList.add(tempDate)
                }
                else {
                    if (weekCount == r.FrequencyR) {
                        r.recurReminderList.add(tempDate)
                    }
                }
            }
        }
        Timber.i("remind_you - Next reminder: ${r.RecurNextReminder}, List: ${r.recurReminderList}")

        /* Old code
        if (firstReminder) {
            r.RecurNextReminder = r.RecurStartDate
            r.recurReminderList.add(r.RecurNextReminder)

            tempDate = LocalDate.ofEpochDay((r.RecurStartDate.toEpochDay() + 365))
            rOccurrencesWeeks = if (r.RecurEndDate.isAfter(tempDate))
                ((tempDate.toEpochDay() - r.RecurStartDate.toEpochDay()) / 7 / r.FrequencyR)
            else
                ((r.RecurEndDate.toEpochDay() - r.RecurStartDate.toEpochDay()) / 7 / r.FrequencyR)

            tempDate = r.RecurStartDate
            for (x in 1.until((rOccurrencesWeeks))) {
                if (r.RecurDays.count() > 1) {
                    if (tempDate.dayOfWeek == DayOfWeek.SUNDAY) {
                        tempDate = tempDate.plusWeeks(r.FrequencyR.toLong() - 1)
                    }
                    do {
                        tempDate = tempDate.plusDays(1)
                        if (r.RecurDays.contains(tempDate.dayOfWeek)) {
                            r.recurReminderList.add(tempDate)
                        }
                    } while (tempDate.dayOfWeek != r.RecurDays[0])
                    tempDate.plusWeeks(r.FrequencyR.toLong())
                    r.recurReminderList.add(tempDate)
                } else {
                    tempDate = tempDate.plusWeeks(r.FrequencyR.toLong())
                    r.recurReminderList.add(tempDate)
                }
            }
        } else {
            val iterThrough = mutableListOf<LocalDate>()
            iterThrough.addAll(r.recurReminderList)
            for (x in iterThrough) {
                if (x <= LocalDate.now() || r.RecurNextReminder == x) {
                    r.recurReminderList.remove(x)
                    continue
                } else {
                    r.RecurNextReminder = x
                    break
                }
            }

            tempDate = r.recurReminderList.last()

            do {
                if (r.RecurDays.count() > 1) {
                    if (tempDate.dayOfWeek == DayOfWeek.SUNDAY) {
                        tempDate = tempDate.plusWeeks(r.FrequencyR.toLong() - 1)
                    }
                    do {
                        tempDate = tempDate.plusDays(1)
                        if (r.RecurDays.contains(tempDate.dayOfWeek)) {
                            r.recurReminderList.add(tempDate)
                        }
                    } while (tempDate.dayOfWeek != r.RecurDays[0])
                    tempDate.plusWeeks(r.FrequencyR.toLong())
                    r.recurReminderList.add(tempDate)
                } else {
                    tempDate = tempDate.plusWeeks(r.FrequencyR.toLong())
                    r.recurReminderList.add(tempDate)
                }
            } while (tempDate.isBefore(r.RecurEndDate) && tempDate.isBefore(
                    r.RecurNextReminder.plusDays(
                        365
                    )
                )
            )
        } */
    }

    /**
     * Sets recurring next reminder dates for monthly recurring up to 365 days ahead
     * TODO: Needs to be tested.
     *
     * @param r - recurring reminder
     * @param firstReminder - is this the first time the reminder is being created?
     */
    private fun setMonthlyRecurReminders(r: Reminder, firstReminder: Boolean = false) {
        var rOccurrences: Long
        var tempDate: LocalDate
        var recurNextReminderSet = false
        var leapYear = LocalDate.now().isLeapYear

        if (r.RecurEndDate.isBefore(LocalDate.now())
            || (r.RecurEndDate == LocalDate.now() && r.RecurNoticeTime.isBefore(LocalTime.now()))
        ) {
            r.ReminderStatus = 2
            return
        }

        if (r.RecurNextReminder.isAfter(LocalDate.now())) {
            return
        }

        if (
            r.RecurNextReminder == LocalDate.now()
            && r.RecurNoticeTime.isAfter(LocalTime.now())
        )
            return

        if (firstReminder) {
            r.RecurNextReminder = r.RecurStartDate
        }
        else {
            r.RecurNextReminder = r.recurReminderList[0]
            recurNextReminderSet = true
            r.recurReminderList.clear()
        }

        var tempDayOfMonth = 0
        while (!recurNextReminderSet) {
            if (r.RecurNextReminder.month.length(leapYear) < r.DateOfMonth)
                tempDayOfMonth = r.RecurNextReminder.month.length(leapYear)
            else
                tempDayOfMonth = 0
            if (r.RecurNextReminder.dayOfMonth == r.DateOfMonth
                || r.RecurNextReminder.dayOfMonth == tempDayOfMonth) {
                recurNextReminderSet = true
            }
            else {
                r.RecurNextReminder = r.RecurNextReminder.plusDays(1)
            }
        }

        tempDate = r.RecurNextReminder
        tempDayOfMonth = 0
        var monthCount = 0
        while (r.recurReminderList.isEmpty()) {
            tempDate = tempDate.plusDays(1)
            if (tempDate.month.length(leapYear) < r.DateOfMonth)
                tempDayOfMonth = tempDate.month.length(leapYear)
            else
                tempDayOfMonth = 0

            if (tempDate.dayOfMonth == 1) monthCount++
            if (tempDate.dayOfMonth == r.DateOfMonth
                || tempDate.dayOfMonth == tempDayOfMonth) {
                if (monthCount == 0){
                    r.recurReminderList.add(tempDate)
                }
                else {
                    if (monthCount == r.FrequencyR) {
                        r.recurReminderList.add(tempDate)
                    }
                }
            }
        }

        Timber.i("remind_you - Next Reminder: ${r.RecurNextReminder}, List: ${r.recurReminderList}")

        /* old code
        if (firstReminder) {
            r.RecurNextReminder = r.RecurStartDate
            r.recurReminderList.add(r.RecurNextReminder)

            if (r.DateOfMonth != 0) {
                tempDate = LocalDate.ofEpochDay((r.RecurStartDate.toEpochDay() + 365))

                //Here we determine how many occurrences are in a year
                if (r.RecurEndDate.isAfter(tempDate)) {
                    rOccurrences = (12 / r.FrequencyR.toLong())
                } else {
                    if (r.RecurEndDate.year > r.RecurStartDate.year)
                        rOccurrences =
                            ((r.RecurEndDate.monthValue + 12 - r.RecurStartDate.monthValue) / r.FrequencyR.toLong())
                    else
                        rOccurrences =
                            ((r.RecurEndDate.monthValue - r.RecurStartDate.monthValue) / r.FrequencyR.toLong())
                }
                //Now we add our occurrences to the reminder list
                tempDate = r.RecurNextReminder
                for (x in 1.until(rOccurrences)) {
                    tempDate = tempDate.plusMonths(r.FrequencyR.toLong())
                    //This next part is to check if the previous month had a lesser number of days than
                    //the date of month. If so and our new month has the appropriate number, we will
                    //increment the day. So if last month is 28 - new month is 30, we add 2 days 30-28
                    // 31, 28, 30
                    if (tempDate.dayOfMonth != r.DateOfMonth)
                        tempDate =
                            tempDate.plusDays(tempDate.lengthOfMonth() - tempDate.dayOfMonth.toLong())
                }
            } else {
                tempDate = r.RecurStartDate

                do {
                    tempDate = tempDate.plusDays(1)
                    if ((tempDate.dayOfMonth + 7 - 1) / 7 == r.WeekOfMonth && r.RecurDays.contains(
                            tempDate.dayOfWeek
                        )
                    ) {
                        r.recurReminderList.add(tempDate)
                        continue
                    } else if (r.RecurDays.contains(tempDate.dayOfWeek) &&
                        (tempDate.dayOfMonth + 7 - 1) / 7 == r.WeekOfMonth - 1 &&
                        r.WeekOfMonth == 5
                    ) {

                        if (tempDate.plusWeeks(1).monthValue != tempDate.monthValue) {
                            r.recurReminderList.add(tempDate)
                            continue
                        }
                    } else if (tempDate.minusDays(1).monthValue != tempDate.monthValue) {
                        tempDate = tempDate.plusMonths(r.FrequencyR.toLong() - 1)
                    }

                } while (tempDate.isBefore(r.RecurNextReminder.plusDays(365)))
            }
        } else {
            val iterThrough = mutableListOf<LocalDate>()
            iterThrough.addAll(r.recurReminderList)
            for (x in iterThrough) {
                if (x <= LocalDate.now() || r.RecurNextReminder == x) {
                    r.recurReminderList.remove(x)
                    continue
                } else {
                    r.RecurNextReminder = x
                    break
                }
            }
            tempDate = r.recurReminderList.last()
            do {
                if (r.DateOfMonth != 0) {
                    //Here we determine how many occurrences are in a year
                    if (r.RecurEndDate.isAfter(tempDate.plusDays(365))) {
                        rOccurrences = (12 / r.FrequencyR.toLong())
                    } else {
                        if (r.RecurEndDate.year > tempDate.year)
                            rOccurrences =
                                ((r.RecurEndDate.monthValue + 12 - tempDate.monthValue) / r.FrequencyR.toLong())
                        else
                            rOccurrences =
                                ((r.RecurEndDate.monthValue - tempDate.monthValue) / r.FrequencyR.toLong())
                    }
                    //Now we add our occurrences to the reminder list
                    for (x in 1.until(rOccurrences)) {
                        tempDate = tempDate.plusMonths(r.FrequencyR.toLong())
                        //This next part is to check if the previous month had a lesser number of days than
                        //the date of month. If so and our new month has the appropriate number, we will
                        //increment the day. So if last month is 28 - new month is 30, we add 2 days 30-28
                        // 31, 28, 30
                        if (tempDate.dayOfMonth != r.DateOfMonth)
                            tempDate =
                                tempDate.plusDays(tempDate.lengthOfMonth() - tempDate.dayOfMonth.toLong())
                    }
                } else {
                    do {
                        tempDate = tempDate.plusDays(1)
                        if ((tempDate.dayOfMonth + 7 - 1) / 7 == r.WeekOfMonth && r.RecurDays.contains(
                                tempDate.dayOfWeek
                            )
                        ) {
                            r.recurReminderList.add(tempDate)
                            continue
                        } else if (r.RecurDays.contains(tempDate.dayOfWeek) &&
                            (tempDate.dayOfMonth + 7 - 1) / 7 == r.WeekOfMonth - 1 &&
                            r.WeekOfMonth == 5
                        ) {

                            if (tempDate.plusWeeks(1).monthValue != tempDate.monthValue) {
                                r.recurReminderList.add(tempDate)
                                continue
                            }
                        } else if (tempDate.minusDays(1).monthValue != tempDate.monthValue) {
                            tempDate = tempDate.plusMonths(r.FrequencyR.toLong() - 1)
                        }

                    } while (tempDate.isBefore(r.RecurNextReminder.plusDays(365)) && tempDate.isBefore(
                            r.RecurEndDate
                        )
                    )
                }
            } while (tempDate.isBefore(r.RecurEndDate) && tempDate.isBefore(
                    r.RecurNextReminder.plusDays(
                        365
                    )
                )
            )
        }*/
    }

    /**
     * Sets the yearly recurring reminders next recurring dates up to 365 days.
     *
     *
     * @param r - recurring reminder
     * @param firstReminder - is this the first time this reminder is being created?
     */
    private fun setYearlyRecurReminders(r: Reminder, firstReminder: Boolean = false) {
        var tempDate = LocalDate.now()
        var recurNextReminderSet = false

        if (r.RecurEndDate.isBefore(LocalDate.now())
            || (r.RecurEndDate == LocalDate.now() && r.RecurNoticeTime.isBefore(LocalTime.now()))
        ) {
            r.ReminderStatus = 2
            return
        }

        if (r.RecurNextReminder.isAfter(LocalDate.now())) {
            return
        }

        if (
            r.RecurNextReminder == LocalDate.now()
            && r.RecurNoticeTime.isAfter(LocalTime.now())
        )
            return

        if (firstReminder) {
            r.RecurNextReminder = r.RecurStartDate
        }
        else {
            r.RecurNextReminder = r.recurReminderList[0]
            recurNextReminderSet = true
            r.recurReminderList.clear()
        }

        var tempDayOfMonth = 0
        while (!recurNextReminderSet) {
            if (r.RecurNextReminder.month.length(r.RecurNextReminder.isLeapYear) < r.DateOfMonth)
                tempDayOfMonth = r.RecurNextReminder.month.length(r.RecurNextReminder.isLeapYear)
            else
                tempDayOfMonth = 0
            if (r.RecurNextReminder.dayOfMonth == r.DateOfMonth
                || r.RecurNextReminder.dayOfMonth == tempDayOfMonth) {
                recurNextReminderSet = true
            }
            else {
                r.RecurNextReminder = r.RecurNextReminder.plusDays(1)
            }
        }

        tempDate = r.RecurNextReminder
        tempDayOfMonth = 0
        var yearCount = 0
        while (r.recurReminderList.isEmpty()){
            tempDate = tempDate.plusDays(1)
            if (tempDate.month.length(tempDate.isLeapYear) < r.DateOfMonth)
                tempDayOfMonth = tempDate.month.length(tempDate.isLeapYear)
            else
                tempDayOfMonth = 0

            if (tempDate.dayOfYear == 1) {
                yearCount++
                while (yearCount != r.FrequencyR) {
                    tempDate.plusYears(1)
                    yearCount++
                    if (yearCount > r.FrequencyR)
                    {
                        Timber.e("remind_you - unexpected pass of yearly frequency")
                        break
                    }
                }
            }
            if (r.RecurMonths.contains(tempDate.month)) {
                if (yearCount == 0) {
                    if (tempDate.dayOfMonth == r.DateOfMonth ||
                        tempDate.dayOfMonth == tempDayOfMonth
                    ) {
                        r.recurReminderList.add(tempDate)
                    }
                }
                else if (yearCount == r.FrequencyR){
                    if (tempDate.dayOfMonth == r.DateOfMonth ||
                        tempDate.dayOfMonth == tempDayOfMonth
                    ) {
                        r.recurReminderList.add(tempDate)
                    }
                }
                else {
                    Timber.e("remind_you - something unexpected happened with yearly frequency")
                    break
                }
            }
        }

        /* old code
        if (firstReminder) {
            r.RecurNextReminder = r.RecurStartDate
            r.recurReminderList.add(r.RecurNextReminder)

            tempDate = r.RecurNextReminder

            do {
                tempDate = tempDate.plusMonths(1)
                if (r.FrequencyR == 1) {
                    if (r.RecurMonths.contains(tempDate.month)) {
                        r.recurReminderList.add(tempDate)
                        continue
                    }
                } else {
                    if (tempDate.year != tempDate.minusMonths(1).year)
                        break
                }
            } while (tempDate.isBefore(r.RecurNextReminder.plusDays(365)))
        } else {
            val iterThrough = mutableListOf<LocalDate>()
            iterThrough.addAll(r.recurReminderList)
            for (x in iterThrough) {
                if (r.FrequencyR > 1 && r.RecurNextReminder.month == r.RecurMonths.last()) {
                    while (r.RecurNextReminder.month != r.RecurMonths[0])
                        r.RecurNextReminder.minusMonths(1)
                    r.RecurNextReminder = r.RecurNextReminder.plusYears(r.FrequencyR.toLong())
                    if (r.RecurNextReminder.isAfter(r.RecurEndDate)) {
                        r.ReminderStatus = 2
                        return
                    }
                    r.recurReminderList.add(r.RecurNextReminder)
                    r.recurReminderList.remove(x)
                } else if (x <= LocalDate.now() || r.RecurNextReminder == x) {
                    r.recurReminderList.remove(x)
                    if (x != r.RecurNextReminder)
                        r.recurReminderList.add(x.plusYears(r.FrequencyR.toLong()))
                    continue
                } else {
                    r.recurReminderList.add(x.plusYears(r.FrequencyR.toLong()))
                    r.RecurNextReminder = x
                    break
                }
            }
        }*/
    }
}

/**
 * Data Class Reminder
 *
 * This holds the structure of a Reminder in the RemindYOU app - only to be accessed by the ReminderEngine
 * @property ReminderID - ReminderID. This should only be set by the google apps engine.
 * @property OwnerID - OwnerID of the reminder
 * @property UserIDs - Array list of User IDs that have access to reminder (Shared)
 * @property GroupIDs - Groups that have access to the reminder (shared)
 * @property ReminderTitle - Title of the reminder
 * @property ReminderDescription - Description of the reminder
 * @property ReminderStatus - Status of the reminder; 0 = inactive, 1 = active, 2 = complete
 * @property ReminderTag - User generated tags for the reminder; ie: shopping, sports !!CURRENTLY NOT IN USE
 * @property ReminderLocation - Location where the reminder should take place !!CURRENTLY NOT IN USE
 * @property CreationDate - Date reminder was created; done by ReminderEngine. Stored as YYYY-MM-DD
 * @property CreationTime - Time reminder was created; done by ReminderEngine. Stored as HH:MM:SS.sss
 * @property CompletionDate - Complete Date is a User set completion date of the reminder derived from the edit screen. stored YYYY-MMM-DD
 * @property CompletionTime - Complete Time is the time a user would like to be alerted. Stored as HH:MM:SS.sss
 * @property ReminderRecurType - This tells us what type of reminder this is. 0 = normal, 1 = recurring daily, 2 = recuring weekly, 3 = recurring monthly, 4 = recurring yearly
 * @property FrequencyR - tells us the frequency of alerts based on recur type (example if its weekly recur type and this is 2 - alert every 2 weeks)
 * @property _id - this is the MongoDB object id. This should only be set by the google apps engine.
 * @property RecurStartDate - this is the start date of a recurring reminder
 * @property RecurEndDate - this is the last date of a recurring reminder
 * @property RecurNoticeTime - time of RecurDays to set off notification
 * @property RecurDays - mutable list of DayOfTheWeek enum. Holds days of the week reminder should go off. Used for recurring reminders. //TODO: change so on monthly you can only choose one day and last / first
 * @property RecurMonths - mutable list of MonthOfYear enum. Holds months that the reminder should go off. Used for recurring reminders.
 * @property DateOfMonth - integer array that holds the RecurDays of the RecurMonths 1-31.
 * @property WeekOfMonth - integer array that holds the week of the RecurMonths 1-5. 1 = frist occurrence of a RecurDays. So if the RecurDays list is Mon and this is
 * set to 1, then the first monday will trigger the reminder. 6 is considered last. So if there are 4 occurrences of a RecurDays (max) then it will trigger on
 * the 5th. Also if there are 4 occurrences, it will trigger on the 4th. 4th is not the same and will only trigger the 4th occurrence. //TODO: change this to occurrence like first, second, third, fourth, last
 * @property RecurNextReminder - LocalDate that holds the value the next time the Recurring Reminder should go off
 * @property [reminderJSONObject] - holds the JSON version of a single reminder.
 */
data class Reminder(
    var ReminderID: Int = 0,
    var OwnerID: Int = 0,
    var UserIDs: IntArray = intArrayOf(0),
    var GroupIDs: IntArray = intArrayOf(0),
    var ReminderTitle: String = "Add Title!",
    var ReminderDescription: String = "Add Description!",
    var ReminderStatus: Int = 0,
    var ReminderTag: String = "Add Type!",
    var ReminderLocation: String = "Add Location",
    var CreationDate: LocalDate = LocalDate.now(),
    var CreationTime: LocalTime = LocalTime.now(),
    var CompletionDate: LocalDate = LocalDate.of(2000, 1, 1),
    var CompletionTime: LocalTime = LocalTime.of(1, 0),
    var ReminderRecurType: Int = 0,
    var _id: String = " ",
    var RecurStartDate: LocalDate = LocalDate.of(2000,1,1),
    var RecurEndDate: LocalDate = LocalDate.of(2000,1,1),
    var FrequencyR: Int = 0,
    var RecurNoticeTime: LocalTime = LocalTime.now(),
    val RecurDays: MutableList<DayOfWeek> = mutableListOf(),
    val RecurMonths: MutableList<Month> = mutableListOf(),
    var DateOfMonth: Int = 0,
    var WeekOfMonth: Int = 0,
    var RecurNextReminder: LocalDate = LocalDate.of(2000,1,1)
) {
    val recurReminderList: MutableList<LocalDate> = mutableListOf()
    val reminderJSONObject = JSONObject()


    /**
     * Overrides Kotlin equals. Needed to change how UserIDs and GroupIDs are compared in the dataclass
     *
     * @param other - <Any?>  Reminder object to be evaluated against
     * @return - true if equals, false if not.
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Reminder
        if (_id != other._id ||
            ReminderID != other.ReminderID ||
            OwnerID != other.OwnerID ||
            !UserIDs.contentEquals(other.UserIDs) ||
            !GroupIDs.contentEquals(other.GroupIDs) ||
            ReminderTitle != other.ReminderTitle ||
            ReminderDescription != other.ReminderDescription ||
            ReminderStatus != other.ReminderStatus ||
            ReminderTag != other.ReminderTag ||
            ReminderLocation != other.ReminderLocation ||
            CreationDate != other.CreationDate ||
            CreationTime != other.CreationTime ||
            CompletionDate != other.CompletionDate ||
            ReminderRecurType != other.ReminderRecurType ||
            FrequencyR != other.FrequencyR ||
            RecurStartDate != other.RecurStartDate ||
            RecurEndDate != other.RecurEndDate ||
            RecurNoticeTime != other.RecurNoticeTime ||
            !RecurDays.containsAll(other.RecurDays) ||
            !RecurMonths.containsAll(other.RecurMonths) ||
            DateOfMonth != other.DateOfMonth ||
            WeekOfMonth != other.WeekOfMonth ||
            RecurNextReminder != other.RecurNextReminder ||
            recurReminderList.containsAll(other.recurReminderList) ||
            reminderJSONObject.toString() != other.reminderJSONObject.toString()
        ) return false

        return true
    }

    fun setAs(other: Reminder) {
        ReminderID = other.ReminderID
        OwnerID = other.OwnerID
        UserIDs = other.UserIDs
        GroupIDs = other.GroupIDs
        ReminderTitle = other.ReminderTitle
        ReminderDescription = other.ReminderDescription
        ReminderStatus = other.ReminderStatus
        ReminderTag = other.ReminderTag
        ReminderLocation = other.ReminderLocation
        CreationDate = other.CreationDate
        CreationTime = other.CreationTime
        CompletionDate = other.CompletionDate
        ReminderRecurType = other.ReminderRecurType
        FrequencyR = other.FrequencyR
        RecurStartDate = other.RecurStartDate
        RecurEndDate = other.RecurEndDate
        RecurNoticeTime = other.RecurNoticeTime
        RecurDays.clear()
        RecurDays.addAll(other.RecurDays)
        RecurMonths.clear()
        RecurMonths.addAll(other.RecurMonths)
        DateOfMonth = other.DateOfMonth
        WeekOfMonth = other.WeekOfMonth
        RecurNextReminder = other.RecurNextReminder
        recurReminderList.clear()
        recurReminderList.addAll(other.recurReminderList)
    }

    /**
     * Override of Kotlin hashCode. Intended to fix UserIDs and GroupIDs Array Hashing
     * normal hashCode functionality will only hash the memory pointing to Arrays and not the contents
     *
     * @return - Returns Int
     */
    override fun hashCode(): Int {
        var hash = 0

        hash += ReminderID.hashCode()
        hash += OwnerID.hashCode()
        hash += UserIDs.contentHashCode()
        hash += GroupIDs.contentHashCode()
        hash += ReminderTitle.hashCode()
        hash += ReminderDescription.hashCode()
        hash += ReminderStatus.hashCode()
        hash += ReminderTag.hashCode()
        hash += ReminderLocation.hashCode()
        hash += CreationDate.hashCode()
        hash += CreationTime.hashCode()
        hash += CompletionDate.hashCode()
        hash += CompletionTime.hashCode()
        hash += ReminderRecurType.hashCode()
        hash += FrequencyR.hashCode()
        hash += _id.hashCode()
        hash += reminderJSONObject.hashCode()
        hash += RecurStartDate.hashCode()
        hash += RecurEndDate.hashCode()
        hash += RecurNoticeTime.hashCode()
        RecurDays.forEach {
            hash += it.hashCode()
        }
        RecurMonths.forEach {
            hash += it.hashCode()
        }
        hash += DateOfMonth.hashCode()
        hash += WeekOfMonth.hashCode()
        hash += RecurNextReminder.hashCode()
        hash += reminderJSONObject.toString().hashCode()
        recurReminderList.forEach {
            hash += it.hashCode()
        }

        return hash
    }

    companion object {
        val REMINDER_COMPLETE = 2
        val REMINDER_ACTIVE = 1
        val REMINDER_INACTIVE = 0
    }

}

