package iris.technology.remindingyou

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_report_bug.*
import timber.log.Timber


class ReportBug : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_report_bug, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val imm : InputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        etBugDescription.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus)
                imm.hideSoftInputFromWindow(view.windowToken,0)
        }
        bBugAccept.setOnClickListener {
            sendBugReport()
            Navigation.findNavController(view).popBackStack()
        }
        bBugCancel.setOnClickListener {
            Navigation.findNavController(view).popBackStack()
        }
    }

    private fun sendBugReport(){
        Timber.i("sending bug report to travis.torcivia@gmail.com")
        val TO_EMAIL = arrayOf<String>("travis.torcivia@gmail.com")
        val emailIntent : Intent = Intent(Intent.ACTION_SEND)
            .setData(Uri.parse("mailto:"))
            .setType("text/plain")
            .putExtra(Intent.EXTRA_EMAIL,TO_EMAIL)
            .putExtra(Intent.EXTRA_SUBJECT,"RemindYou BUG REPORT")
            .putExtra(Intent.EXTRA_TEXT,etBugDescription.text.toString())

        try {
            startActivity(Intent.createChooser(emailIntent, "Send Mail..."))
        }
        catch (ex : android.content.ActivityNotFoundException) {
            Toast.makeText(activity, "There is no email client installed!",Toast.LENGTH_SHORT).show()
        }

    }
}