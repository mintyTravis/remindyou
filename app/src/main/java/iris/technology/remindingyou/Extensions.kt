package iris.technology.remindingyou

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import timber.log.Timber

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    Timber.i("ViewGroup.inflate extension method called")
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}
