package iris.technology.remindingyou

import android.app.Application
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.DialogFragment
import com.jakewharton.threetenabp.AndroidThreeTen
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalTime
import org.threeten.bp.Month
import timber.log.Timber

/**
 * Global application override as set in the AndroidManifest.xml
 * Used to extend application types (activities and fragments)
 * Sets the Timber plant so each area can use timber logging.
 * Also sets the ThreeTen date so we can use threeten LocalDate / LocalTime
 *
 * This class needs to be used if something should be initiated application wide
 *
 */
class GlobalApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant (Timber.DebugTree())
        AndroidThreeTen.init(this)
    }

}

/**
 * Time Picker dialog fragment. This launches the standard time picker fragment.
 * Creates and interface 'onTimePickerEnd'.
 * To use extend fragment / activity with TimePickerFragment.setTimePickerSelectedListener and
 * override onTimePickerEnd
 *
 * @property c - LocalTime property that is modified by the method that declares this class.
 */
class TimePickerFragment(private var c:LocalTime) : DialogFragment(), TimePickerDialog.OnTimeSetListener {
    private lateinit var callback: OnTimePickerSelectedListener
    private lateinit var editText: EditText

    fun setTimePickerSelectedListener(callback: OnTimePickerSelectedListener, etField: EditText){
        Timber.i("setTimePickerSelectedListener method called")
        this.callback = callback
        this.editText = etField

    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        Timber.i("onCreateDialog method called")

        return TimePickerDialog(context!!,this, c.hour, c.minute,false)
    }

    override fun onTimeSet(view: TimePicker, hourOfDay: Int, min: Int) {
        Timber.i("onTimeSet method called")
        c = LocalTime.of(hourOfDay,min)
        callback.onTimePickerEnd(hourOfDay,min, editText,c)
    }

    interface OnTimePickerSelectedListener {
        fun onTimePickerEnd(hourOfDay: Int,min: Int, etField: EditText,c: LocalTime)
    }

}

/**
 * Date Picker fragment that launches android standard date picker. To use interface, extend fragment
 * or activity with DatePickerFragment.setDatePickerSelectedListener and override onDatePickerEnd
 *
 * @property c - LocalDate that is modified when class is constructed.
 */
class DatePickerFragment(var c:LocalDate) : DialogFragment(), DatePickerDialog.OnDateSetListener {
    private lateinit var callback: OnDatePickerSelectedListener
    private lateinit var editText: EditText


    fun setDatePickerSelectedListener(callback: OnDatePickerSelectedListener,etField: EditText) {
        Timber.i("setDatePickerSelectedListener method called")
        this.callback = callback
        this.editText = etField
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        Timber.i("onCreateDialog method called")
        return DatePickerDialog(context!!,this,c.year,c.monthValue-1,c.dayOfMonth)
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, dayOfMonth: Int) {
        Timber.i("onDateSet method called")
        c = LocalDate.of(year, month+1, dayOfMonth)
        callback.onDatePickerEnd(editText,c)
    }
    interface OnDatePickerSelectedListener {
        fun onDatePickerEnd(editText: EditText,c: LocalDate)
    }

}

/**
 * RecurTypeDialog frag that runs the dialog fragment - uses layout - recur_type_popout.
 * Extend fragment with setRecurTypePickerListener to use interface. Override onRecurPickerEnd
 *
 * @property recurType - int that is modified and passed back with constructor
 */
class RecurTypeDialogFrag(private val recurType: Int) : DialogFragment() {
    private val TAG = "RecurTypeDialogFrag"
    private lateinit var callback: OnRecurTypeSelectedListener
    var daily: Boolean = false
    var weekly: Boolean = false
    var monthly: Boolean = false
    var yearly: Boolean = false
    var none: Boolean = false

    /**
     * method used to set the listener in the fragment
     *
     * @param callback
     */
    fun setRecurTypePickerListener (callback: OnRecurTypeSelectedListener) {
        this.callback = callback
    }

    /**
     * Interface that needs to extend fragment
     *
     */
    interface OnRecurTypeSelectedListener {
        fun onRecurPickerEnd(daily: Boolean, weekly: Boolean, Monthly: Boolean, yearly: Boolean, none: Boolean )
    }

    /**
     * Creates view from recur_type_popout
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Timber.i("onCreateView method called")
        return inflater.inflate(R.layout.recur_type_popout,container,false)
    }

    /**
     * Sets listeners as well as what the current checkmark should be
     *
     * @param view
     * @param savedInstanceState
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.i("onViewCreated method called")
        val bfinish: Button = view.findViewById(R.id.bFinish2)
        val rbDaily = view.findViewById<RadioButton>(R.id.rbDaily)
        val rbWeekly = view.findViewById<RadioButton>(R.id.rbWeekly)
        val rbMonthly = view.findViewById<RadioButton>(R.id.rbMonthly)
        val rbYearly = view.findViewById<RadioButton>(R.id.rbYearly)
        val rbNone = view.findViewById<RadioButton>(R.id.rbNone)
        val rgDailyWeekly = view.findViewById<RadioGroup>(R.id.rgDailyWeekly)
        val rgYearlyNone = view.findViewById<RadioGroup>(R.id.rgYearlyNone)

        rbDaily.isChecked = (recurType == 1)
        rbWeekly.isChecked = (recurType == 2)
        rbMonthly.isChecked = (recurType == 3)
        rbYearly.isChecked = (recurType == 4)
        rbNone.isChecked = (recurType == 0)


        rbDaily.setOnClickListener {
            Timber.i("Radio Group Daily")
            if (rbMonthly.isChecked) rbMonthly.isChecked = false
            if (rgYearlyNone.checkedRadioButtonId != -1) rgYearlyNone.clearCheck()
        }

        rbYearly.setOnClickListener {
            Timber.i("Radio Group Yearly")
            if (rbMonthly.isChecked) rbMonthly.isChecked = false
            if (rgDailyWeekly.checkedRadioButtonId != -1) rgDailyWeekly.clearCheck()
        }

        rbWeekly.setOnClickListener {
            if (rbMonthly.isChecked) rbMonthly.isChecked = false
            if (rgYearlyNone.checkedRadioButtonId != -1) rgYearlyNone.clearCheck()
        }

        rbNone.setOnClickListener {
            if (rbMonthly.isChecked) rbMonthly.isChecked = false
            if (rgDailyWeekly.checkedRadioButtonId != -1) rgDailyWeekly.clearCheck()
        }

        bfinish.setOnClickListener {
            dialog?.dismiss()
            daily = rbDaily.isChecked
            weekly = rbWeekly.isChecked
            monthly = rbMonthly.isChecked
            yearly = rbYearly.isChecked
            none = rbNone.isChecked
        }

        rbMonthly.setOnClickListener {
            Timber.i("Radio Button Monthly")
            rbMonthly.isChecked = true
            if (rgDailyWeekly.checkedRadioButtonId != -1) rgDailyWeekly.clearCheck()
            if (rgYearlyNone.checkedRadioButtonId != -1) rgYearlyNone.clearCheck()
        }

    }

    /**
     * Currently only running the super method. This does nothing but logs using timber.
     *
     * @param savedInstanceState
     * @return
     */
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        Timber.i("onCreateDialog Method Called")
        return super.onCreateDialog(savedInstanceState)
    }

    /**
     * Runs the callback onRecurPickerEnd.
     *
     * @param dialog
     */
    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        callback.onRecurPickerEnd(daily,weekly,monthly,yearly,none)
    }
}

/**
 * ReminderStatusDialog - launches the reminder status dialog picker using active_options_popout layout.
 * Extend fragment with OnReminderStatusSelectListener.
 *
 * @property reminderStatus
 */
class ReminderStatusDialog(private val reminderStatus: Int) : DialogFragment() {
    private val TAG = "ReminderStatusDialog"
    private lateinit var callback: OnReminderStatusSelectListener
    private lateinit var editText: EditText
    private var etString = " "

    /**
     * Sets listener in fragment
     *
     * @param callback
     * @param editText
     */
    fun setOnReminderStatusListener(callback: OnReminderStatusSelectListener, editText: EditText){
        this.callback = callback
        this.editText = editText

    }

    /**
     * Interface to extend fragment.
     *
     */
    interface OnReminderStatusSelectListener {
        fun onReminderStatusPickerEnd(string: String, etField: EditText)
    }

    /**
     * Inflates view with active_options_popout layout.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.active_options_popout,container,false)
    }

    /**
     * Sets button lsitener and current status of the reminder.
     *
     * @param view
     * @param savedInstanceState
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bFinished = view.findViewById<Button>(R.id.bFinish2)
        val rbActive = view.findViewById<RadioButton>(R.id.rbActive)
        val rbComplete = view.findViewById<RadioButton>(R.id.rbCompleted)
        val rbInactive = view.findViewById<RadioButton>(R.id.rbInactive)

        rbActive.isChecked = (reminderStatus == 1)
        rbComplete.isChecked =  (reminderStatus == 2)
        rbInactive.isChecked = (reminderStatus == 0)


        bFinished.setOnClickListener {
            if (rbActive.isChecked) etString = "Active"
            if (rbComplete.isChecked) etString = "Complete"
            if (rbInactive.isChecked) etString = "Inactive"

            this.dismiss()
        }

    }

    /**
     * Dismisses dialog and runs callback onReminderStatusPickerEnd override
     *
     * @param dialog
     */
    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        this.callback.onReminderStatusPickerEnd(etString,editText)
    }

}

/**
 * Receives mutable list from fragment that sets on constructor. Runs Dialog fragment from recur_days_popout
 * layout. Extend fragment using SetOnClickDaysPickerListener
 *
 * @property days
 */
class DaysDialogPicker(private val days: MutableList<DayOfWeek>): DialogFragment() {
    private lateinit var callback: SetOnClickDaysPickerListener
    private lateinit var editText: EditText
    private val pickerDays = mutableListOf<DayOfWeek>()

    /**
     * Used to extend fragment.
     *
     */
    interface SetOnClickDaysPickerListener {
        fun onDaysDialogPickerEnd(days: MutableList<DayOfWeek>, editText: EditText)
    }

    /**
     * Set the callback listener in fragment.
     *
     * @param callback
     * @param editText
     */
    fun setOnClickDaysPickerListener(callback: SetOnClickDaysPickerListener, editText: EditText) {
        this.callback = callback
        this.editText = editText
    }

    /**
     * Creates view using recur_days_popout and sets current days
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.recur_days_popout,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bFinish = view.findViewById<Button>(R.id.bFinishDaysPicker)
        val sun = view.findViewById<CheckBox>(R.id.cbSUN)
        val mon = view.findViewById<CheckBox>(R.id.cbMON)
        val tue = view.findViewById<CheckBox>(R.id.cbTUE)
        val wed = view.findViewById<CheckBox>(R.id.cbWED)
        val thu = view.findViewById<CheckBox>(R.id.cbTHU)
        val fri = view.findViewById<CheckBox>(R.id.cbFRI)
        val sat = view.findViewById<CheckBox>(R.id.cbSAT)

        sun.isChecked = days.contains(DayOfWeek.SUNDAY)
        mon.isChecked = days.contains(DayOfWeek.MONDAY)
        tue.isChecked = days.contains(DayOfWeek.TUESDAY)
        wed.isChecked = days.contains(DayOfWeek.WEDNESDAY)
        thu.isChecked = days.contains(DayOfWeek.THURSDAY)
        fri.isChecked = days.contains(DayOfWeek.FRIDAY)
        sat.isChecked = days.contains(DayOfWeek.SATURDAY)

        bFinish.setOnClickListener {
            if (sun.isChecked) pickerDays.add(pickerDays.size,DayOfWeek.SUNDAY)
            if (mon.isChecked) pickerDays.add(pickerDays.size,DayOfWeek.MONDAY)
            if (tue.isChecked) pickerDays.add(pickerDays.size,DayOfWeek.TUESDAY)
            if (wed.isChecked) pickerDays.add(pickerDays.size,DayOfWeek.WEDNESDAY)
            if (thu.isChecked) pickerDays.add(pickerDays.size,DayOfWeek.THURSDAY)
            if (fri.isChecked) pickerDays.add(pickerDays.size,DayOfWeek.FRIDAY)
            if (sat.isChecked) pickerDays.add(pickerDays.size,DayOfWeek.SATURDAY)
            this.dismiss()
        }
    }

    /**
     * Runs callback onDaysDialogPickerEnd override in fragment.
     *
     * @param dialog
     */
    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        callback.onDaysDialogPickerEnd(pickerDays,editText)
    }
}

/**
 * MonthsDialogPicker - Uses recur_months_popout layout to display picker.
 * Extend fragment with SetOnClickMonthsPickerListener.
 *
 * @property Months - MutableList<Months> used to set months in fragment
 */
class MonthsDialogPicker (private val Months: MutableList<Month>): DialogFragment() {
    private lateinit var callback: SetOnClickMonthsPickerListener
    private lateinit var editText: EditText
    private val pickerMonths = mutableListOf<Month>()

    /**
     * Extend fragment with this interface
     *
     */
    interface SetOnClickMonthsPickerListener {
        fun onMonthsDialogPickerEnd(Months: MutableList<Month>, editText: EditText)
    }

    /**
     * Set the on Picker listener in the fragment.
     *
     * @param callback
     * @param editText
     */
    fun setOnClickMonthsPickerListener(callback: SetOnClickMonthsPickerListener, editText: EditText) {
        this.callback = callback
        this.editText = editText
    }

    /**
     * Inflates view with recur_months_popout layout. Sets current selected months.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.recur_months_popout,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bFinish = view.findViewById<Button>(R.id.bFinishMonthPicker)
        val jan = view.findViewById<CheckBox>(R.id.cbJAN)
        val feb = view.findViewById<CheckBox>(R.id.cbFEB)
        val mar = view.findViewById<CheckBox>(R.id.cbMAR)
        val apr = view.findViewById<CheckBox>(R.id.cbAPR)
        val may = view.findViewById<CheckBox>(R.id.cbMAY)
        val jun = view.findViewById<CheckBox>(R.id.cbJUN)
        val jul = view.findViewById<CheckBox>(R.id.cbJUL)
        val aug = view.findViewById<CheckBox>(R.id.cbAUG)
        val sep = view.findViewById<CheckBox>(R.id.cbSEP)
        val oct = view.findViewById<CheckBox>(R.id.cbOCT)
        val nov = view.findViewById<CheckBox>(R.id.cbNOV)
        val dec = view.findViewById<CheckBox>(R.id.cbDEC)

        jan.isChecked = Months.contains(Month.JANUARY)
        feb.isChecked = Months.contains(Month.FEBRUARY)
        mar.isChecked = Months.contains(Month.MARCH)
        apr.isChecked = Months.contains(Month.APRIL)
        may.isChecked = Months.contains(Month.MAY)
        jun.isChecked = Months.contains(Month.JUNE)
        jul.isChecked = Months.contains(Month.JULY)
        aug.isChecked = Months.contains(Month.AUGUST)
        sep.isChecked = Months.contains(Month.SEPTEMBER)
        oct.isChecked = Months.contains(Month.OCTOBER)
        nov.isChecked = Months.contains(Month.NOVEMBER)
        dec.isChecked = Months.contains(Month.DECEMBER)

        bFinish.setOnClickListener {
            if (jan.isChecked) pickerMonths.add(pickerMonths.size,Month.JANUARY)
            if (feb.isChecked) pickerMonths.add(pickerMonths.size,Month.FEBRUARY)
            if (mar.isChecked) pickerMonths.add(pickerMonths.size,Month.MARCH)
            if (apr.isChecked) pickerMonths.add(pickerMonths.size,Month.APRIL)
            if (may.isChecked) pickerMonths.add(pickerMonths.size,Month.MAY)
            if (jun.isChecked) pickerMonths.add(pickerMonths.size,Month.JUNE)
            if (jul.isChecked) pickerMonths.add(pickerMonths.size,Month.JULY)
            if (aug.isChecked) pickerMonths.add(pickerMonths.size,Month.AUGUST)
            if (sep.isChecked) pickerMonths.add(pickerMonths.size,Month.SEPTEMBER)
            if (oct.isChecked) pickerMonths.add(pickerMonths.size,Month.OCTOBER)
            if (nov.isChecked) pickerMonths.add(pickerMonths.size,Month.NOVEMBER)
            if (dec.isChecked) pickerMonths.add(pickerMonths.size,Month.DECEMBER)
            this.dismiss()
        }
    }

    /**
     * Runs the callback onMonthsDialogPickerEnd override in fragment.
     *
     * @param dialog
     */
    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        callback.onMonthsDialogPickerEnd(pickerMonths,editText)
    }
}