package iris.technology.remindingyou


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import iris.technology.remindingyou.databinding.ActivityMainBinding
import timber.log.Timber
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {
    private lateinit var drawerLayout: DrawerLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.i("onCreate method called")
        @Suppress("UNUSED_VARIABLE")
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        drawerLayout = binding.drawerLayout

        val navController = this.findNavController(R.id.nav_host_fragment)
        NavigationUI.setupActionBarWithNavController(this,navController,drawerLayout)
        NavigationUI.setupWithNavController(binding.navView,navController)
    }


    override fun onSupportNavigateUp(): Boolean {
        Timber.i("onSupportNavigateUp method called")
        val navController = this.findNavController(R.id.nav_host_fragment)
        return NavigationUI.navigateUp(navController,drawerLayout)
    }

    //Called instead of onCreate if app was not destroyed but stopped.
    override fun onRestart() {
        super.onRestart()
        Timber.i("onRestart method called")
    }

    //Called after onCreate
    override fun onStart() {
        super.onStart()
        Timber.i("onStart method called")
        NotificationUtils().setDailyNotificationCheck(this)

    }

    //Called after onStart
    override fun onResume() {
        super.onResume()
        Timber.i("onResume method called")
        if (intent != null) {
            Timber.i("remind_you - intent is not null")
            Timber.i(intent.getIntExtra("ReminderID",0).toString())
        }
    }

    //Called first when app goes to foreground or is closed
    override fun onPause() {
        super.onPause()
        Timber.i("onPause method called")
    }

    //Called after onPause - activity is not visible
    override fun onStop() {
        super.onStop()
        Timber.i("onStop method called")
    }

    //Called after onStop - activity is closed.
    override fun onDestroy() {
        Timber.i("onDestroy method called")
        super.onDestroy()

    }

    //Can be used to save data if OS kills the app if its needs more resources for foreground apps
    //Data is saved as 'Dictionary' key / data - then recreated on onCreate or onRestoreInstanceState
    //
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Timber.i("onSaveInstanceState method called")
    }

    //Can be used to retrieve data back from a bundle created in onSavedInstanceState
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        Timber.i("onRestoreInstanceState method called")
    }

}
