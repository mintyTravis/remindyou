package iris.technology.remindingyou


import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_add_reminder_text.*
import java.util.*
import iris.technology.remindingyou.databinding.FragmentAddReminderTextBinding
import timber.log.Timber

/**
 * A simple [Fragment] subclass.
 */
class AddReminderText : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val viewRoot: FragmentAddReminderTextBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_reminder_text, container, false)
        viewRoot.bCancel.setOnClickListener {
            Navigation.findNavController(it).popBackStack()
        }
        viewRoot.bComplete.setOnClickListener {
            Navigation.findNavController(it).popBackStack()
        }

        viewRoot.imageView2.setOnClickListener {
            getSpeechInput(it)
        }


        return viewRoot.root

    }

    fun getSpeechInput(view: View) {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())

        if (intent.resolveActivity(activity!!.packageManager) != null)
            startActivityForResult(intent, 10)
        else
            Toast.makeText(activity,"Your device does not support Voice to Text",Toast.LENGTH_LONG).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            10 ->
                if (resultCode == RESULT_OK && data != null) {
                    val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                    etReminderBoxText.setText(result[0])
                }
            else
                    Timber.d("here")
        }
    }
}
