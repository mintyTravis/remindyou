package iris.technology.remindingyou

import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import kotlinx.coroutines.delay
import org.json.JSONArray
import org.json.JSONObject
import org.threeten.bp.LocalDate
import timber.log.Timber
import kotlin.concurrent.thread

/**
 * This is the UserEngine. This will handle all back end user loging and store current user data
 * while the app is running. Communicates and makes changes with database.
 * TODO: Need to handle silentSignInBetter - some type of progress timer while we wait for new token?
 *
 */
object UserEngine {
    private val TAG = "UserEngine"
    private var activeUser: GoogleSignInAccount? = null
    private var singleUserInstance = UserClass()
    private lateinit var volleyQueue: RequestQueue
    private lateinit var callback: VolleyCallback
    private lateinit var mGoogleSignInClient: GoogleSignInClient

    /**
     * Extend fragement or activity to interface with the volley callbacks
     *
     */
    interface VolleyCallback {
        fun onSuccessResponseUser(result: Boolean)
    }

    fun onVolleyReturnListener(callback: VolleyCallback){
        this.callback = callback
    }

    /**
     * This is the method that will initiate the user. It will send the current user information we
     * have to the server and retrieve back what the server has. Used for both new and current
     * users
     *
     * @param act
     * @return - returns false if unsuccessful
     */
    fun initiateUser(act: GoogleSignInAccount?) {
        if (act != null) {
            activeUser = act
        } else {
            Timber.e("Something went wrong! - initiateUser")
            return
        }
        updateSingleUserInstance()
        Timber.i(
            """Display Name: ${activeUser?.displayName}
                |Family Name: ${activeUser?.familyName}
                |Email: ${activeUser?.email}
                |ServerAuth: ${activeUser?.serverAuthCode}
                |TokenID: ${activeUser?.idToken}""".trimMargin()
        )
        volleyUserToMongo()
    }

    /**
     * TODO
     *
     * @return
     */
    fun returnUserID(): Int{
        return singleUserInstance.userID
    }

    fun returnGoogleToken(): String? {
        return singleUserInstance.googleToken
    }

    /**
     * TODO
     *
     * @param callback
     */
    private fun volleyUserToMongo(){
        convertUserJSONobject()
        val url = "https://remind-you-1.appspot.com/users/login"
        // Test Link
        //val url = "http://192.168.1.174:5050/users/login"
        val volleyInitiateUser = JsonObjectRequest(
            Request.Method.POST,
            url,
            singleUserInstance.jsonObject,
            Response.Listener {
                Timber.d("$it")
                if (it.has("UserID")) {
                    singleUserInstance.userID = it.getInt("UserID")
                    Timber.d("volleyUserToMongo: ${singleUserInstance.userID}")
                    callback.onSuccessResponseUser(true)
                }
                else{
                    callback.onSuccessResponseUser(false)
                }
            },
            Response.ErrorListener {
                callback.onSuccessResponseUser(false)
            })
        volleyQueue.add(volleyInitiateUser)
        volleyQueue.start()
    }

    private fun updateSingleUserInstance() {
        val nameString = activeUser?.displayName?.split(' ')
        var firstname: String? = null
        var middlename: String? = null
        var lastname: String? = null

        if (nameString != null) {
            if (nameString.count() == 3) {
                firstname = nameString[0]
                middlename = nameString[1]
                lastname = nameString[2]
            } else if (nameString.count() == 2) {
                firstname = nameString[0]
                middlename = null
                lastname = nameString[1]
            } else {
                firstname = nameString[0]
                middlename = null
                lastname = null
            }
        }
        singleUserInstance = UserClass(
            firstname, middlename, lastname, activeUser?.email, activeUser?.idToken, userStatus = 1
        )
    }

    fun revokeAccess(){
        activeUser = null
        singleUserInstance.clear()
    }

    fun userSignOut(){
        activeUser = null
        singleUserInstance.clear()
    }

    private fun createSilentSignIn(context: Context): Boolean{

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken("173150357767-ie9lque65p9p5jbqhv3ijvo4kfqj2q4f.apps.googleusercontent.com")
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(context, gso)
        mGoogleSignInClient.silentSignIn()
            .addOnCompleteListener {
                activeUser = it.result
                Timber.d("addOnCompleteListener Complete")
                if (activeUser != null) {
                    updateSingleUserInstance()
                    initiateVolley(context)
                    volleyUserToMongo()
                }

            }
            .addOnFailureListener {
                Timber.d(it)
            }
        return true
    }

    fun removeUser(act: GoogleSignInAccount?) {
        singleUserInstance = UserClass(
            null, null, null, null, null,
            null, null, null, null, null
        )
    }

    fun checkForLoggedInUser(context: Context): Boolean {
        //TODO: There may be a bug here that displays incorrect toast in ReminderClass callback when a user signs in via google login. -- after they hit the back button
        if (activeUser == null) activeUser = GoogleSignIn.getLastSignedInAccount(context)
        if (activeUser != null) {
            if (activeUser!!.isExpired) {
                if (!createSilentSignIn(context)) return false
            }
            else if (singleUserInstance.userID == 0) {
                updateSingleUserInstance()
                initiateVolley(context)
                volleyUserToMongo()
            }
            else {
                callback.onSuccessResponseUser(true)
            }
        }
        else {
            return false
        }
        return true
    }

    fun initiateVolley(context: Context) {
        volleyQueue = Volley.newRequestQueue(context)
    }

    private fun convertUserJSONobject() {
        val arrayGroups = JSONArray()
        singleUserInstance.jsonObject.put("UserID", singleUserInstance.userID)
        singleUserInstance.jsonObject.putOpt("Email", singleUserInstance.userEmail)
        singleUserInstance.jsonObject.putOpt("Password", singleUserInstance.userPassword)
        singleUserInstance.jsonObject.putOpt("FirstName", singleUserInstance.userFirstName)
        singleUserInstance.jsonObject.putOpt("MiddleName", singleUserInstance.userMiddleName)
        singleUserInstance.jsonObject.putOpt("LastName", singleUserInstance.userLastName)
        singleUserInstance.jsonObject.putOpt("ActivationDate", singleUserInstance.userMiddleName)
        singleUserInstance.jsonObject.putOpt("DeactivationDate", singleUserInstance.userMiddleName)
        singleUserInstance.jsonObject.putOpt("UserStatus", singleUserInstance.userStatus)
        if (singleUserInstance.groupIDs != null)
            for (x in singleUserInstance.groupIDs!!) arrayGroups.put(x)
        singleUserInstance.jsonObject.put("GroupIDs", arrayGroups)
        singleUserInstance.jsonObject.putOpt("GoogleToken", singleUserInstance.googleToken)
        Timber.d("convertUserJSONObject: ${singleUserInstance.jsonObject}")
    }

}

/**
 * Dataclass for users.
 *
 * @property userFirstName
 * @property userMiddleName
 * @property userLastName
 * @property userEmail
 * @property googleToken
 * @property userPassword
 * @property activationDate
 * @property deactivationDate
 * @property userStatus
 * @property groupIDs
 * @property userID
 */
data class UserClass(
    var userFirstName: String? = null,
    var userMiddleName: String? = null,
    var userLastName: String? = null,
    var userEmail: String? = null,
    var googleToken: String? = null,
    var userPassword: String? = null,
    var activationDate: LocalDate? = null,
    var deactivationDate: LocalDate? = null,
    var userStatus: Int? = null,
    var groupIDs: IntArray? = null,
    var userID: Int = 0
) {
    val jsonObject = JSONObject()

    fun clear() {
        userFirstName = null
        userMiddleName = null
        userLastName = null
        userEmail = null
        googleToken = null
        userPassword = null
        activationDate = null
        deactivationDate = null
        userStatus = null
        groupIDs = null
        userID = 0
    }
}