package iris.technology.remindingyou

import android.content.Context
import android.content.Intent
import android.graphics.*
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import iris.technology.remindingyou.databinding.FragmentRemindersMainBinding
import kotlinx.android.synthetic.main.fragment_reminders_main.*
import kotlinx.android.synthetic.main.rv_layout_recurringreminders.view.*
import kotlinx.android.synthetic.main.rv_layout_todaysreminder.view.*
import org.threeten.bp.*
import org.threeten.bp.format.DateTimeFormatter
import timber.log.Timber
import java.util.*


/**
 * The main fragment. This is where the reminders are displayed. Todays, Upcoming, Recurring, Shared.
 * This is the fragment where all navigation should happen.
 *
 */
class RemindersMain : Fragment(), ReminderEngine.VolleyCallback,UserEngine.VolleyCallback {
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var linearLayoutManagerU: LinearLayoutManager
    private lateinit var linearLayoutManagerR: LinearLayoutManager
    private lateinit var linearLayoutManagerS: LinearLayoutManager
    private lateinit var rvAdapterToday: ReminderAdapterToday
    private lateinit var rvAdapterUpcoming: ReminderAdapterUpcoming
    private lateinit var rvAdapterRecurring: ReminderAdapterRecurring
    private lateinit var rvAdapterShared: ReminderAdapterShared
    private var isFabMenuOpen = false
    private val p = Paint()


    /**
     * Sets the fragment view to fragment_reminders_main
     * Sets the layout managers and adapters for recycler view
     * Sets the click listeners
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Timber.i("onCreateView method called")
        val binder: FragmentRemindersMainBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_reminders_main, container, false)

        linearLayoutManager = LinearLayoutManager(activity)
        linearLayoutManagerR = LinearLayoutManager(activity)
        linearLayoutManagerS = LinearLayoutManager(activity)
        linearLayoutManagerU = LinearLayoutManager(activity)

        binder.rvTodaysReminders.layoutManager = linearLayoutManager
        binder.rvUpcomingReminders.layoutManager = linearLayoutManagerU
        binder.rvRecurringReminders.layoutManager = linearLayoutManagerR
        binder.rvSharedReminders.layoutManager = linearLayoutManagerS

        rvAdapterToday = ReminderAdapterToday(ReminderEngine.reminderListToday)
        rvAdapterUpcoming = ReminderAdapterUpcoming(ReminderEngine.reminderListUpcoming)
        rvAdapterRecurring = ReminderAdapterRecurring(ReminderEngine.reminderListRecurring)
        rvAdapterShared = ReminderAdapterShared(ReminderEngine.reminderListShared)



        setHasOptionsMenu(true)

        binder.bottomAppBar.setNavigationOnClickListener {
            binder.bottomAppBar.setNavigationOnClickListener(null)
            getReminders()
        }

        binder.fab.setOnClickListener { _ ->
            showFabMenu()

            //Navigation.findNavController(view)
             //   .navigate(R.id.action_remindersMain_to_editReminderFrag2)
        }
        return binder.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvTodaysReminders.adapter = rvAdapterToday
        rvUpcomingReminders.adapter = rvAdapterUpcoming
        rvRecurringReminders.adapter = rvAdapterRecurring
        rvSharedReminders.adapter = rvAdapterShared
    }

    private fun showFabMenu(){
        isFabMenuOpen = true
        Timber.d("showFabMenu")

        fab_menu.animate().alpha(1f)
            .setUpdateListener {
                fab_menu.isClickable = true
                fab_menu.animate().setUpdateListener(null)
            }
        fab_edit_reminder.show()
        fab_add_text.show()
        tvEditReminder.visibility = View.VISIBLE
        tvAddTextReminder.visibility = View.VISIBLE

        fab.animate().rotation(135f)

        fab_edit_reminder.animate()
            .rotation(0f)
            .translationX((-15).toFloat())

        fab_add_text.animate()
            .rotation(0f)
            .translationX((-15).toFloat())

        fab_edit_reminder.setOnClickListener {
            Navigation.findNavController(it)
                .navigate(R.id.action_remindersMain_to_editReminderFrag2)
            //closeFabMenu()
        }

        fab_add_text.setOnClickListener {
            Navigation.findNavController(it)
                .navigate(R.id.action_remindersMain_to_addReminderText)
            //closeFabMenu()
        }

        fab_menu.setOnClickListener {
            closeFabMenu()
            it.isClickable = false
        }

    }

    private fun closeFabMenu() {
        isFabMenuOpen = false
        Timber.d("cloase fab menu")

        fab_edit_reminder.animate()
            .translationY(0f)
            .rotation(90f)
            .setUpdateListener {
                fab_edit_reminder.hide()
                tvEditReminder.visibility = View.GONE
                    fab_edit_reminder.animate().setUpdateListener(null)
            }

        fab_add_text.animate()
            .translationY(0f)
            .rotation(90f)
            .setUpdateListener {
                Timber.d("fab_listener")
                fab_add_text.hide()
                fab_menu.animate().alpha(0f)
                tvAddTextReminder.visibility = View.GONE
                fab.animate()
                    .rotation(0f)
                fab_add_text.animate().setUpdateListener(null)
            }
    }

    /**
     * Inflates overflow menu using the overflow_menu layout
     *
     * @param menu
     * @param inflater
     */
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        Timber.i("onCreateOptionsMenu method called")
        inflater.inflate(R.menu.overflow_menu, menu)
    }

    /**
     * Sets the navigations for option items
     *
     * @param item
     * @return
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        Timber.i("onOptionsItemSelected method called")
        return NavigationUI.onNavDestinationSelected(item,view!!.findNavController())
        || super.onOptionsItemSelected(item)
    }

    /**
     * Tells the ReminderEngine to retrieve all the users reminders from the database.
     *
     */
    private fun getReminders() {
        Timber.i("remind_you - getReminders method called")
        UserEngine.onVolleyReturnListener(this)
        if (!UserEngine.checkForLoggedInUser(activity!!)) {
            val intent = Intent(activity, GoogleLogin::class.java).apply {
            }
            startActivity(intent)
        }
        else {
            progressBarMainFrag.visibility = View.VISIBLE
        }

    }

    /**
     * Interface with reminder engine - when successful retrival of reminders from database.
     * Sets the Recycler Adapters and refreshes them.
     *
     * @param result
     */
    override fun onSuccessResponse(result: String){
        Timber.i("remind_you - onSuccessResponse called")
        var timeInMilli: Long = 0

        cleanOutdatedReminders()


        rvAdapterToday.notifyDataSetChanged()
        rvAdapterUpcoming.notifyDataSetChanged()
        rvAdapterRecurring.notifyDataSetChanged()
        rvAdapterShared.notifyDataSetChanged()

        for (x in ReminderEngine.reminderListToday) {
            if (x.CompletionTime > LocalTime.now()) {
                timeInMilli =
                    x.CompletionTime.atDate(LocalDate.now()).atZone(ZoneId.systemDefault())
                        .toInstant().toEpochMilli()
                Timber.d("timeInMili: $timeInMilli - calendar time in mili: ${Calendar.getInstance().timeInMillis}")
                if (timeInMilli > 0) NotificationUtils().setNotification(timeInMilli, activity!!, x)
            }
            if (x.ReminderRecurType > 0){
                if (x.RecurNoticeTime > LocalTime.now()){
                    timeInMilli = x.RecurNoticeTime.atDate(LocalDate.now()).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()
                    NotificationUtils().setNotification(timeInMilli, activity!!,x)
                }
            }
        }

        progressBarMainFrag.visibility = View.GONE
        bottomAppBar.setNavigationOnClickListener {
            bottomAppBar.setNavigationOnClickListener(null)
            getReminders()
        }
        initSwipe()
    }

    private fun initSwipe(){


        val swipeToComplete = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT/*.or(ItemTouchHelper.RIGHT)*/) {
            var adapterString = ""

            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {

                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position: Int = viewHolder.adapterPosition

                if (adapterString.contains("ReminderAdapterToday")) {
                    ReminderEngine.completeReminder(this@RemindersMain.context!!,rvAdapterToday.getReminder(position))
                    rvAdapterToday.removeReminder(position)
                } else if (adapterString.contains("ReminderAdapterUpcoming")){
                    ReminderEngine.completeReminder(this@RemindersMain.context!!,rvAdapterUpcoming.getReminder(position))
                    rvAdapterUpcoming.removeReminder(position)
                } else if (adapterString.contains("ReminderAdapterRecurring")) {
                    ReminderEngine.completeReminder(this@RemindersMain.context!!,rvAdapterRecurring.getReminder(position))
                    rvAdapterRecurring.removeReminder(position)
                } else if (adapterString.contains("ReminderAdapterShared")) {
                    ReminderEngine.completeReminder(this@RemindersMain.context!!,rvAdapterShared.getReminder(position))
                    rvAdapterShared.removeReminder(position)
                } else {
                    Timber.e("Unable to identify ReminderAdapter.")
                }
            }

            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {
                val icon: Bitmap
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE){

                    adapterString = recyclerView.adapter.toString()

                    val itemView: View = viewHolder.itemView
                    val height: Float = itemView.bottom.toFloat() - itemView.top.toFloat()
                    val width: Float = height / 3

                    if (dX < 0){

                        p.color = Color.parseColor("#388E3C")
                        val background: RectF = RectF(itemView.right.toFloat() + dX, itemView.top.toFloat(),itemView.right.toFloat(), itemView.bottom.toFloat())
                        c.drawRect(background,p)
                        icon = BitmapFactory.decodeResource(resources, R.drawable.baseline_done_outline_black_18dp)
                        val iconDest = RectF(itemView.right.toFloat() - 2*width, itemView.top.toFloat() + width, itemView.right - width, itemView.bottom.toFloat() - width)
                        c.drawBitmap(icon,null,iconDest,null)

                    } else if (dX > 0) {

                        p.color = Color.parseColor("#D32F2F")
                        val background: RectF = RectF(itemView.left.toFloat(), itemView.top.toFloat(), dX, itemView.bottom.toFloat())
                        c.drawRect(background,p)
                        icon = BitmapFactory.decodeResource(resources, R.drawable.baseline_done_black_18dp)
                        val icon_dest = RectF(itemView.left.toFloat() + width, itemView.top.toFloat() + width, itemView.left + 2*width, itemView.bottom.toFloat() - width)
                        c.drawBitmap(icon,null,icon_dest,p)
                    }
                }
                super.onChildDraw(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
            }
        }

        //initializing the touch helper with SwipeToComplete object - attaching each to their
        //respective recycler views.
        val itemTouchHelperT = ItemTouchHelper(swipeToComplete)
        itemTouchHelperT.attachToRecyclerView(rvTodaysReminders)

        val itemTouchHelperU = ItemTouchHelper(swipeToComplete)
        itemTouchHelperU.attachToRecyclerView(rvUpcomingReminders)

        val itemTouchHelperR = ItemTouchHelper(swipeToComplete)
        itemTouchHelperR.attachToRecyclerView(rvRecurringReminders)

        val itemTouchHelperS = ItemTouchHelper(swipeToComplete)
        itemTouchHelperS.attachToRecyclerView(rvSharedReminders)

    }

    override fun onStart() {
        super.onStart()
        Timber.i("remind_you - onStart method called")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Timber.i("remind_you - onAttach method called")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.i("remind_you - onCreate method called")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Timber.i("remind_you - onActivityCreated method called")
    }

    private fun cleanOutdatedReminders(){
        Timber.i("remind_you - cleanOutdatedReminders called")
        val sr = ReminderEngine.singleReminderInstance
        if (ReminderEngine.reminderListFull.isNotEmpty()) {
            for (x in ReminderEngine.reminderListFull) {
                if (x.ReminderRecurType == 0) {
                    if (x.CompletionDate < LocalDate.now()) {
                        x.ReminderStatus = 2
                        sr.setAs(x)
                        ReminderEngine.cleanReminder(activity!!,x)
                    }
                }
                else{
                    if (x.RecurEndDate < LocalDate.now()) {
                        x.ReminderStatus = 2
                        sr.setAs(x)
                        ReminderEngine.cleanReminder(activity!!, x)
                    }
                }
            }
        }
    }

    override fun onSuccessResponseUser(result: Boolean) {
        Timber.i("remind_you - onSuccessResponseUserCalled")
        if (result) {
            ReminderEngine.getAllReminders(activity!!, this)
        }
        else Toast.makeText(activity,"Something Went Wrong!!",Toast.LENGTH_LONG).show()
    }

    /**
     * Sets Username on the text view user name using the login display name.
     *
     */
    override fun onResume() {
        super.onResume()
        Timber.i("remind_you - onResume called")
        if (activity?.intent != null){
            Timber.i("remind_you - notification intent from activity: ${activity!!.intent.getIntExtra("ReminderID",0)}")
            Timber.i("remind_you - intent action: ${activity!!.intent.action}")
            Timber.i("remind_you - intent package: ${activity!!.intent.`package`}")
        }
        UserEngine.onVolleyReturnListener(this)
        if (!UserEngine.checkForLoggedInUser(activity!!)) {
            val intent = Intent(activity, GoogleLogin::class.java).apply {
            }
            startActivity(intent)
        }
        else{
            progressBarMainFrag.visibility = View.VISIBLE
        }
    }

    override fun onPause() {
        super.onPause()
        Timber.i("remind_you - onPause method called")
    }

    override fun onStop() {
        super.onStop()
        Timber.i("remind_you - onStop method called")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Timber.i("remind_you - onDestroyView method called")
    }
}

/**
 * Uses reminderList from ReminderEngine to display today's reminders.
 *
 * @property remindersList
 */
class ReminderAdapterToday(private val remindersList: MutableList<Reminder> = mutableListOf()): RecyclerView.Adapter<ReminderAdapterToday.ReminderHolder>() {
    override fun getItemCount() = remindersList.size


    /**
     * Sets the position of the reminder recycler item
     *
     * @param holder
     * @param position
     */
    override fun onBindViewHolder(holder: ReminderHolder, position: Int) {
        val whichReminder = remindersList[position]
        holder.showReminder(whichReminder)
    }

    /**
     * Inflates the item view using rv_layout_todaysreminder
     *
     * @param parent
     * @param viewType
     * @return
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReminderHolder {
        Timber.i("remind_you - onCreateViewHolder")
        val inflatedView = parent.inflate(R.layout.rv_layout_todaysreminder,false)
        return ReminderHolder((inflatedView))
    }

    /**
     * subclass for reminder adapter. Creates the holder for the item view.
     *
     * @constructor
     *
     *
     * @param itemsView
     */
    class ReminderHolder(itemsView: View): RecyclerView.ViewHolder(itemsView), View.OnClickListener {
        private var view = itemsView
        private var reminder = Reminder()

        /**
         * Sets an on click listener
         *
         *
         */
        init {
            itemsView.setOnClickListener(this)
        }

        /**
         * Sends reminder ID to the EditReminderFrag so that EditReminderFrag can populate fields with
         * reminder that was clicked on.
         *
         * @param v
         */
        override fun onClick(v: View?) {
            Timber.d("${this.reminder.ReminderID}")
            val passReminderID = RemindersMainDirections.actionRemindersMainToEditReminderFrag2(this.reminder.ReminderID)
            v?.findNavController()?.navigate(passReminderID)
        }

        fun returnRecycler(): Int {
            return 1
        }

        /**
         * Companion object that can be used with ReminderHolder.
         *
         */
        companion object {
            private val REMINDER_KEY = "REMINDER"
        }

        /**
         * Sets the current reminder item with information from reminderList
         *
         * @param reminder
         */
        fun showReminder(reminder: Reminder) {
            this.reminder = reminder
            if(reminder.ReminderRecurType == 0) {
                view.tvReminderTitle_TodaysReminders.text = reminder.ReminderTitle
                view.tvReminderDescription_TodaysReminders.text = reminder.ReminderDescription
                view.tvCompleteTime_TodaysReminders.text =
                    reminder.CompletionTime.format(DateTimeFormatter.ofPattern("hh:mm a"))
                view.tvCompleteDate_TodaysReminders.text =
                    reminder.CompletionDate.format(DateTimeFormatter.ofPattern("M/dd"))
            }
            else {
                view.tvReminderTitle_TodaysReminders.text = reminder.ReminderTitle
                view.tvReminderDescription_TodaysReminders.text = reminder.ReminderDescription
                view.tvCompleteTime_TodaysReminders.text =
                    reminder.RecurNoticeTime.format(DateTimeFormatter.ofPattern("hh:mm a"))
                view.tvCompleteDate_TodaysReminders.text =
                    reminder.RecurNextReminder.format(DateTimeFormatter.ofPattern("M/dd"))
            }
        }

    }

    fun getReminder(position: Int) : Reminder{
        return remindersList[position]
    }

    fun removeReminder(position: Int) {
        remindersList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position,remindersList.size)
    }
}

/**
 * Reminder Adapter for reminders upcoming (30 days)
 *
 * @property remindersList
 */
class ReminderAdapterUpcoming(private val remindersList: MutableList<Reminder> = mutableListOf()): RecyclerView.Adapter<ReminderAdapterUpcoming.ReminderHolder>() {
    override fun getItemCount() = remindersList.size


    /**
     * Sets position of current reminder item.
     *
     * @param holder
     * @param position
     */
    override fun onBindViewHolder(holder: ReminderHolder, position: Int) {
        val whichReminder = remindersList[position]
        holder.showReminder(whichReminder)
    }


    /**
     * Inflates using rv_layout_todaysreminder
     * TODO: Create its own layout
     *
     * @param parent
     * @param viewType
     * @return
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReminderHolder {
        val inflatedView = parent.inflate(R.layout.rv_layout_todaysreminder,false)
        return ReminderHolder((inflatedView))
    }

    /**
     * Returns the instance of the reminder at position
     *
     */
    fun getReminder(position: Int) : Reminder{
        return remindersList[position]
    }

    /**
     * Removes the reminder from the current position
     *
     */
    fun removeReminder(position: Int) {
        remindersList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position,remindersList.size)
    }

    /**
     * Holder subclass. This class sets the data of an individual item
     *
     * @constructor
     *
     *
     * @param itemsView
     */
    class ReminderHolder(itemsView: View): RecyclerView.ViewHolder(itemsView), View.OnClickListener {
        private var view = itemsView
        private var reminder = Reminder()

        /**
         * Sets on click listener for item
         */
        init {
            itemsView.setOnClickListener(this)
        }

        /**
         * Method run when item is clicked.
         * Passes Reminder ID to Edit Reminder Frag
         *
         * @param v
         */
        override fun onClick(v: View?) {
            Timber.d("${this.reminder.ReminderID}")
            val passReminderID = RemindersMainDirections.actionRemindersMainToEditReminderFrag2(this.reminder.ReminderID)
            v?.findNavController()?.navigate(passReminderID)
        }

        /**
         * Companion object
         */
        companion object {
            private val REMINDER_KEY = "REMINDER"
        }

        /**
         * Fills out reminder items
         *
         * @param reminder
         */
        fun showReminder(reminder: Reminder) {
            this.reminder = reminder
            if (reminder.ReminderRecurType == 0) {
                view.tvReminderTitle_TodaysReminders.text = reminder.ReminderTitle
                view.tvReminderDescription_TodaysReminders.text = reminder.ReminderDescription
                view.tvCompleteTime_TodaysReminders.text =
                    reminder.CompletionTime.format(DateTimeFormatter.ofPattern("hh:mm a"))
                view.tvCompleteDate_TodaysReminders.text =
                    reminder.CompletionDate.format(DateTimeFormatter.ofPattern("M/dd"))
            }
            else {
                view.tvReminderTitle_TodaysReminders.text = reminder.ReminderTitle
                view.tvReminderDescription_TodaysReminders.text = reminder.ReminderDescription
                view.tvCompleteTime_TodaysReminders.text =
                    reminder.RecurNoticeTime.format(DateTimeFormatter.ofPattern("hh:mm a"))
                view.tvCompleteDate_TodaysReminders.text =
                    reminder.RecurNextReminder.format(DateTimeFormatter.ofPattern("M/dd"))
            }
        }

    }
}

/**
 * ReminderAdapter for recurring reminders.
 *
 * @property remindersList
 */
class ReminderAdapterRecurring(private val remindersList: MutableList<Reminder> = mutableListOf()): RecyclerView.Adapter<ReminderAdapterRecurring.ReminderHolder>() {
    override fun getItemCount() = remindersList.size


    /**
     * Sets position of reminder.
     *
     * @param holder
     * @param position
     */
    override fun onBindViewHolder(holder: ReminderHolder, position: Int) {
        val whichReminder = remindersList[position]
        holder.showReminder(whichReminder)
    }

    /**
     * Inflates item with rv_layout_todaysreminder
     * TODO: Create its own recuring layout
     *
     * @param parent
     * @param viewType
     * @return
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReminderHolder {
        val inflatedView = parent.inflate(R.layout.rv_layout_recurringreminders,false)
        return ReminderHolder((inflatedView))
    }

    /**
     * Returns the reminder at a given position
     */
    fun getReminder(position: Int) : Reminder{
        return remindersList[position]
    }

    /**
     * Removes the reminder at a given position
     */
    fun removeReminder(position: Int) {
        remindersList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position,remindersList.size)
    }

    /**
     * Subclass that handles the data of each item.
     *
     * @constructor
     *
     *
     * @param itemsView
     */
    class ReminderHolder(itemsView: View): RecyclerView.ViewHolder(itemsView), View.OnClickListener {
        private var view = itemsView
        private var reminder = Reminder()

        /**
         * sets on click listeners
         */
        init {
            itemsView.setOnClickListener(this)
        }

        /**
         * Run when a reminder is clicked
         *
         * @param v
         */
        override fun onClick(v: View?) {
            Timber.d("${this.reminder.ReminderID}")
            val passReminderID = RemindersMainDirections.actionRemindersMainToEditReminderFrag2(this.reminder.ReminderID)
            v?.findNavController()?.navigate(passReminderID)
        }
        companion object {
            private val REMINDER_KEY = "REMINDER"
        }

        /**
         * Sets text with remidner data.
         *
         * @param reminder
         */
        fun showReminder(reminder: Reminder) {
            this.reminder = reminder
            var recurString = ""
            view.tvRecur_ReminderTitle.text = reminder.ReminderTitle
            when (reminder.ReminderRecurType) {
                0 -> recurString = "None"
                1 -> recurString = "Daily"
                2 -> recurString = "Weekly"
                3 -> recurString = "Monthly"
                4 -> recurString = "Yearly"
            }
            view.tvRecur_RecurType.text = recurString
            view.tvRecur_NextRemDate.text = reminder.RecurNextReminder.format(DateTimeFormatter.ofPattern("M/dd/yyyy"))
        }

    }
}

/**
 * Adapter for shared reminders.
 *
 * @property remindersList
 */
class ReminderAdapterShared(private val remindersList: MutableList<Reminder> = mutableListOf()): RecyclerView.Adapter<ReminderAdapterShared.ReminderHolder>() {
    override fun getItemCount() = remindersList.size


    /**
     * Sets position of reminder items.
     *
     * @param holder
     * @param position
     */
    override fun onBindViewHolder(holder: ReminderHolder, position: Int) {
        val whichReminder = remindersList[position]
        holder.showReminder(whichReminder)
    }

    /**
     * Inflates reminders with rv_layout_todaysreminder
     * TODO: Create its own layout
     *
     * @param parent
     * @param viewType
     * @return
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReminderHolder {
        val inflatedView = parent.inflate(R.layout.rv_layout_todaysreminder,false)
        return ReminderHolder((inflatedView))
    }

    /**
     * Returns the reminder at a given position
     */
    fun getReminder(position: Int) : Reminder{
        return remindersList[position]
    }

    /**
     * Removes the reminder from a given position
     */
    fun removeReminder(position: Int) {
        remindersList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position,remindersList.size)
    }

    /**
     * Subclass that handles each reminder item
     *
     * @constructor
     *
     *
     * @param itemsView
     */
    class ReminderHolder(itemsView: View): RecyclerView.ViewHolder(itemsView), View.OnClickListener {
        private var view = itemsView
        private var reminder = Reminder()

        /**
         * sets on click listeners
         */
        init {
            itemsView.setOnClickListener(this)
        }

        /**
         * method run when reminder is clicked.
         * TODO: Set up to navigate to EditSharedReminders
         *
         * @param v
         */
        override fun onClick(v: View?) {
            Timber.d("${this.reminder.ReminderID}")
            val passReminderID = RemindersMainDirections.actionRemindersMainToEditReminderFrag2(this.reminder.ReminderID)
            v?.findNavController()?.navigate(passReminderID)
        }
        companion object {
            private val REMINDER_KEY = "REMINDER"
        }

        /**
         * Sets data for current reminder item.
         *
         * @param reminder
         */
        fun showReminder(reminder: Reminder) {
            this.reminder = reminder
            view.tvReminderTitle_TodaysReminders.text = reminder.ReminderTitle
            view.tvReminderDescription_TodaysReminders.text = reminder.ReminderDescription
            view.tvCompleteTime_TodaysReminders.text = reminder.CompletionTime.toString()
            view.tvCompleteDate_TodaysReminders.text = reminder.CompletionDate.toString()
        }

    }
}